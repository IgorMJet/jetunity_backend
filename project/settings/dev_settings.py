DEBUG = False
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'jetunity',
        'USER': 'jetunity',
        'PASSWORD': '0S`?-!Pq21gNAw4y',
        'HOST': 'localhost',
        'PORT': '5433'
    }
}
ALLOWED_HOSTS = [
    'dev.jet.space',
    'api-dev.jet.space',
    'admin-dev.jet.space'
]
SITE_NAME = 'dev.jet.space'
ADMIN_SITE_URL = 'admin-dev.jet.space'
DEFAULT_FROM_EMAIL = 'robot@%s' % SITE_NAME

CORS_ORIGIN_WHITELIST = [f'https://{x}' for x in ALLOWED_HOSTS] + ['http://localhost:3000', 'http://127.0.0.1:3000']
