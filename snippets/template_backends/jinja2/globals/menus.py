from snippets.choices import StatusChoices
from snippets.template_backends.jinja2 import jinjaglobal
from vars.models import MenuItem


@jinjaglobal
def menu_items(menu_slug):
    return MenuItem.objects.published()\
        .filter(menu__slug=menu_slug, menu__status__exact=StatusChoices.PUBLIC.value)
