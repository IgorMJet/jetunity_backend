from django.db import models
from django.utils import timezone

from ckeditor_uploader.fields import RichTextUploadingField
from image_cropping import ImageCropField

from snippets.models import BaseModel
from snippets.models.abstract import ArticleManager
from snippets.models.image import ImageMixin
from snippets.models.seo import SEOModelMixin


class BaseArticle(SEOModelMixin, ImageMixin, BaseModel):
    """Базовая модель для статей и новостей"""

    title = models.CharField('Заголовок', max_length=255, db_index=True)
    slug = models.SlugField(
        'Алиас', max_length=150, db_index=True, unique=True,
        help_text='Разрешены только латинские символы, цифры, символ подчеркивания и дефис (минус)'
    )
    publish_date = models.DateTimeField(
        'Дата публикации', db_index=True, default=timezone.now, help_text='Можно задать на будущее'
    )
    image = ImageCropField(
        'Изображение', upload_to='articles/%Y/%m/', max_length=255, blank=True, null=True
    )
    list_image = ImageCropField(
        'Изображение в списке', upload_to='articles/%Y/%m/', max_length=255, blank=True, null=True
    )
    image_alt = models.CharField('Alt изображения', max_length=255, blank=True, null=True)

    translation_fields = SEOModelMixin.translation_fields + ('image_alt', 'title')
    objects = ArticleManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class BaseArticleSection(BaseModel):
    """Базовый класс для секций статей"""

    title = models.CharField('Заголовок секции', max_length=255, blank=True, null=True)
    content = RichTextUploadingField('Контент', blank=True, null=True)
    gallery = models.ForeignKey(
        'core.Gallery', verbose_name='Галерея фотографий', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    image_1 = ImageCropField(
        'Изображение 1', upload_to='articles/sections/%Y/%m/', max_length=255,
        blank=True, null=True
    )
    image_2 = ImageCropField(
        'Изображение 2', upload_to='articles/sections/%Y/%m/', max_length=255,
        blank=True, null=True
    )

    translation_fields = ('content', 'title')

    class Meta:
        abstract = True
        verbose_name = 'Секция статьи'
        verbose_name_plural = 'Секции статьи'

    def __str__(self):
        return str(self.pk) if self.pk else ''
