from rest_framework import serializers

from core import models
from snippets.api.serializers import fields


class GalleryPhotoSerializer(serializers.ModelSerializer):

    image = fields.ImageField()
    thumb = fields.ThumbnailField(thumbnail_parameters={'size': '400x300'}, image_field='image')

    class Meta:
        model = models.GalleryPhoto
        fields = ('alt', 'body', 'image', 'thumb')


class GallerySlidesSerializer(serializers.ModelSerializer):

    slides = serializers.SerializerMethodField()

    class Meta:
        model = models.Gallery
        fields = ('slides',)

    @staticmethod
    def get_slides(obj):
        return GalleryPhotoSerializer(obj.photos.published(), many=True).data


class GallerySerializer(serializers.ModelSerializer):

    image = fields.ImageField()
    photos = fields.PublishedRelationField(GalleryPhotoSerializer, many=True)

    class Meta:
        model = models.Gallery
        fields = ('id', 'image', 'photos', 'publish_date', 'title')
