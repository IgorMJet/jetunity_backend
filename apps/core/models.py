from django.db import models
from django.utils import timezone

from ckeditor_uploader.fields import RichTextUploadingField

from snippets.models import BaseModel
from snippets.models.image import ImageMixin


class ReferralCodeFormPageMixin(models.Model):
    """Миксин для страниц с формой реферрала"""

    form_success_message_title = models.CharField(
        'Форма: заголовок сообщения об успешной отправке', max_length=255, blank=True, null=True
    )
    form_success_message_content = RichTextUploadingField(
        'Форма: контент сообщения об успешной отправке', blank=True, null=True
    )
    form_share_button = models.CharField(
        'Форма: кнопка "Поделиться"', max_length=255, blank=True, null=True
    )
    form_copy_button = models.CharField(
        'Форма: кнопка "Скопировать"', max_length=255, blank=True, null=True
    )
    form_sent_to_email = models.TextField(
        'Форма: сообщение об отправке на email', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'form_copy_button', 'form_sent_to_email', 'form_share_button',
        'form_success_message_content', 'form_success_message_title'
    )

    class Meta:
        abstract = True


class Gallery(ImageMixin, BaseModel):
    """Галерея фотографий"""

    title = models.CharField(
        'Рабочее название', max_length=255, db_index=True,
        help_text='Для идентификации в административной части сайта'
    )
    publish_date = models.DateTimeField(
        'Дата публикации', db_index=True, default=timezone.now, help_text='Можно задать на будущее'
    )
    image = models.ImageField(
        'Фотография', upload_to='galleries/images', null=True, blank=True
    )

    translation_fields = ('title',)

    class Meta:
        verbose_name = 'Галерея фотографий'
        verbose_name_plural = 'Галереи фотографий'

    def __str__(self):
        return self.title


class GalleryPhoto(ImageMixin, BaseModel):
    """Фотографии галерей"""

    gallery = models.ForeignKey(
        'core.Gallery', verbose_name='Галерея', related_name='photos', on_delete=models.CASCADE
    )
    image = models.ImageField(
        'Фотография', upload_to='galleries/photos/%Y/%m/%d'
    )
    alt = models.CharField(
        'Текст вместо фото (alt)', blank=True, null=False, max_length=255
    )
    body = RichTextUploadingField(
        'Описание фото', blank=True, null=False
    )

    translation_fields = ('alt', 'body')

    class Meta:
        verbose_name = 'Фотография галереи'
        verbose_name_plural = 'Фотографии галереи'

    def __str__(self):
        return self.alt if self.alt else str(self.pk)
