from modeltranslation.decorators import register

from core import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.Gallery)
class GalleryTranslationOptions(BaseTranslationOptions):
    fields = models.Gallery.translation_fields
    required_languages = {'default': ()}


@register(models.GalleryPhoto)
class GalleryPhotoTranslationOptions(BaseTranslationOptions):
    fields = models.GalleryPhoto.translation_fields
    required_languages = {'default': ()}
