from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'jetsharing'
    verbose_name = 'JetSharing'
