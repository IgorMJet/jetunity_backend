from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField

from core.models import ReferralCodeFormPageMixin
from jetsharing.choices import FlightTypeChoices
from snippets.models import BaseModel, LastModMixin, BasicModel
from snippets.models.pages import BasePage


class JetSharingImport(LastModMixin, BasicModel):
    """Импорт рейсов JetSharing"""

    import_file = models.FileField(
        'XLS-файл',
        upload_to='integrations/jet_sharing/%Y/%m/%d'
    )
    is_processed = models.BooleanField('Обработано', default=False)
    report = models.TextField('Ошибки', blank=True, null=True)

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заявка на импорт рейсов JetSharing'
        verbose_name_plural = 'Импорт рейсов JetSharing'

    def __str__(self):
        return str(self.created.astimezone().strftime('%d.%m.%Y %H:%M:%S'))


class JetSharingFlight(BaseModel):
    """Рейсы"""

    airport_origin = models.ForeignKey(
        'handbooks.Airport', related_name='flights_as_origin',
        verbose_name='Аэропорт вылета', on_delete=models.CASCADE
    )
    airport_destination = models.ForeignKey(
        'handbooks.Airport', related_name='flights_as_destinations',
        verbose_name='Аэропорт прибытия', on_delete=models.CASCADE
    )
    origin_datetime = models.DateTimeField('Время вылета')
    destination_datetime = models.DateTimeField('Время прибытия')
    flight_duration = models.TimeField('Продолжительность полёта')
    plane_model = models.CharField(
        'Модель самолета', max_length=255, blank=True, null=True
    )
    total_seats = models.PositiveSmallIntegerField('Всего мест')
    free_seats = models.PositiveSmallIntegerField('Свободных мест')
    price = models.DecimalField('Цена', max_digits=11, decimal_places=2)
    form_price_hint = models.TextField(
        'Подсказка для цены в списке рейсов', blank=True, null=True
    )
    flight_type = models.CharField(
        'Тип рейса', max_length=100, choices=FlightTypeChoices.choices,
        default=FlightTypeChoices.SHUTTLE.value, blank=True, null=True
    )

    translation_fields = ('form_price_hint',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Рейс'
        verbose_name_plural = 'Рейсы'

    def __str__(self):
        return f'{self.airport_origin}: {self.origin_datetime} -> ' \
               f'{self.airport_destination}: ' \
               f'{self.destination_datetime}'


class JetSharingPage(ReferralCodeFormPageMixin, BasePage):
    """Страница JetSharing"""

    hero_title = models.CharField(
        'Первый экран: заголовок', max_length=255, blank=True, null=True
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='contacts',
        blank=True, null=True
    )
    hero_left_indicator_value = models.CharField(
        'Первый экран: левый показатель - значение', max_length=20, blank=True,
        null=True
    )
    hero_left_indicator_text = models.CharField(
        'Первый экран: левый показатель - текст', max_length=255, blank=True,
        null=True
    )
    hero_right_indicator_text = models.CharField(
        'Первый экран: правый показатель - текст', max_length=255, blank=True,
        null=True
    )
    show_form = models.BooleanField('Показывать блок формы', default=True)
    flights_not_found_text = models.CharField(
        'Текст "рейсы не найдены"', max_length=255, blank=True, null=True
    )
    booking_form_title = models.CharField(
        'Форма бронирования: заголовок формы', max_length=255, blank=True,
        null=True
    )
    show_call_to_action = models.BooleanField(
        'Показывать блок призыва к действию', default=True
    )
    call_to_action_title = models.CharField(
        'Призыв к действию: заголовок', max_length=255, blank=True, null=True
    )
    call_to_action_button_caption = models.CharField(
        'Призыв к действию: текст кнопки', max_length=255, blank=True,
        null=True
    )
    show_sections = models.BooleanField(
        'Показывать блок информационных секций', default=True
    )
    sections_title = models.CharField(
        'Информационные секции: заголовок', max_length=255, blank=True,
        null=True
    )
    show_about = models.BooleanField('Показывать блок об услуге', default=True)
    about_title = models.CharField(
        'Об услуге: заголовок', max_length=255, blank=True, null=True
    )
    about_content = RichTextUploadingField(
        'Об услуге: контент', blank=True, null=True
    )
    about_image = models.ImageField(
        'Об услуге: изображение', max_length=255, blank=True, null=True,
        upload_to='pages/jetsharing'
    )
    about_slogan = models.CharField(
        'Об услуге: слоган', max_length=255, blank=True, null=True
    )
    show_partners = models.BooleanField(
        'Показывать блок партнёров', default=True
    )
    partners_title = models.CharField(
        'Партнёры: заголовок', max_length=255, blank=True, null=True
    )

    form_price_hint = models.TextField(
        'Подсказка для цены в списке рейсов', blank=True, null=True
    )
    form_title = models.CharField(
        'Форма: заголовок', max_length=255, blank=True, null=True
    )
    form_subtitle = models.CharField(
        'Форма: подзаголовок', max_length=255, blank=True, null=True
    )
    form_bg = models.ImageField(
        'Форма: фон', max_length=255, upload_to='referrals/bgs', blank=True,
        null=True
    )
    form_bg_2x = models.ImageField(
        'Форма: фон (двойное качество)', max_length=255,
        upload_to='referrals/bgs', blank=True, null=True
    )

    translation_fields = ReferralCodeFormPageMixin.translation_fields + \
        BasePage.translation_fields + (
            'about_content', 'about_slogan', 'about_title',
            'booking_form_title', 'call_to_action_button_caption',
            'call_to_action_title', 'flights_not_found_text',
            'form_price_hint', 'form_subtitle', 'form_title',
            'hero_left_indicator_text', 'hero_left_indicator_value',
            'hero_right_indicator_text', 'hero_title', 'partners_title',
            'sections_title'
        )

    class Meta:
        verbose_name = 'Страница JetSharing'
        verbose_name_plural = 'Страница JetSharing'

    def __str__(self):
        return 'Страница JetSharing'


class JetSharingSection(BaseModel):
    """Информационные секции"""

    page = models.ForeignKey(
        'jetsharing.JetSharingPage', related_name='sections',
        verbose_name='Страница', on_delete=models.CASCADE
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)
    button_caption = models.CharField(
        'Текст кнопки', max_length=255, blank=True, null=True
    )
    button_url = models.CharField(
        'Ссылка кнопки', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'button_caption', 'button_url', 'content', 'title'
    )

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Информационная секция'
        verbose_name_plural = 'Информационные секции'

    def __str__(self):
        return self.title or str(self.id)


class JetSharingFeature(BaseModel):
    """Возможности"""

    page = models.ForeignKey(
        'jetsharing.JetSharingPage', related_name='features',
        verbose_name='Страница', on_delete=models.CASCADE
    )
    image = models.FileField(
        'Изображение', max_length=255, blank=True, null=True,
        upload_to='pages/jetsharing/features'
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )

    translation_fields = ('title',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Возможность'
        verbose_name_plural = 'Возможности'

    def __str__(self):
        return self.title or str(self.id)


class JetSharingPartner(BaseModel):
    """Партнеры"""

    page = models.ForeignKey(
        'jetsharing.JetSharingPage', related_name='partners',
        verbose_name='Страница', on_delete=models.CASCADE
    )
    image = models.FileField(
        'Изображение', max_length=255, blank=True, null=True,
        upload_to='pages/jetsharing/partners'
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )

    translation_fields = ('title',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'

    def __str__(self):
        return self.title or str(self.id)
