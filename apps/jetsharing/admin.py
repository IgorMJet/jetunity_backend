from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationTabularInline, \
    TranslationStackedInline
from solo.admin import SingletonModelAdmin

from jetsharing import models
from jetsharing.utils.import_flights import import_jetsharing_flights
from snippets.admin import BaseModelAdmin


@admin.register(models.JetSharingImport)
class JetSharingImportAdmin(admin.ModelAdmin):
    """Загрузка рейсов JetSharing"""

    actions = None
    fields = models.JetSharingImport().collect_fields()
    list_display = ('id', 'is_processed', 'created')
    readonly_fields = ('created', 'is_processed', 'report', 'updated')

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.id and obj.is_processed:
            return self.readonly_fields + ('import_file',)

        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        res = super(JetSharingImportAdmin, self).save_model(
            request, obj, form, change
        )
        import_jetsharing_flights(obj, request)
        return res


@admin.register(models.JetSharingFlight)
class JetSharingFlightAdmin(BaseModelAdmin):
    """Рейсы"""

    fields = models.JetSharingFlight().collect_fields()
    list_display = (
        'id', 'airport_origin', 'airport_destination', 'origin_datetime',
        'destination_datetime', 'total_seats', 'flight_type', 'status',
        'ordering', 'created'
    )
    list_display_links = ('id', 'airport_origin', 'airport_destination')
    list_filter = BaseModelAdmin.list_filter + (
        'airport_origin', 'airport_destination', 'airport_origin__city',
        'airport_destination__city', 'origin_datetime', 'destination_datetime',
        'total_seats', 'flight_type'
    )
    list_select_related = True
    search_fields = BaseModelAdmin.search_fields + [
        'airport_origin__code', 'airport_destination__code',
        'airport_origin__city__title', 'airport_destination__city__title',
        'total_seats', 'free_seats', 'price', 'plane_model'
    ]


class JetSharingFeatureInline(TranslationStackedInline):

    extra = 0
    fields = models.JetSharingFeature().collect_fields()
    model = models.JetSharingFeature
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-features'


class JetSharingPartnerInline(TranslationTabularInline):

    extra = 0
    fields = models.JetSharingPartner().collect_fields()
    model = models.JetSharingPartner
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-partners'


class JetSharingSectionInline(TranslationStackedInline):

    extra = 0
    fields = models.JetSharingSection().collect_fields()
    model = models.JetSharingSection
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-sections'


@admin.register(models.JetSharingPage)
class JetSharingPageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Страница JetSharing"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': (
                'hero_title', 'hero_image', 'hero_left_indicator_value',
                'hero_left_indicator_text', 'hero_right_indicator_text'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-booking_form'),
            'fields': (
                'show_form', 'flights_not_found_text', 'booking_form_title',
                'form_price_hint'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-call_to_action'),
            'fields': (
                'show_call_to_action', 'call_to_action_title',
                'call_to_action_button_caption'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-sections'),
            'fields': ('show_sections', 'sections_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-about'),
            'fields': (
                'show_about', 'about_title', 'about_content', 'about_image',
                'about_slogan'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-partners'),
            'fields': ('show_partners', 'partners_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-form'),
            'fields': (
                'form_title', 'form_subtitle', 'form_bg', 'form_bg_2x',
                'form_success_message_title', 'form_success_message_content',
                'form_share_button', 'form_copy_button', 'form_sent_to_email'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (
        JetSharingFeatureInline, JetSharingPartnerInline,
        JetSharingSectionInline
    )
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('booking_form', 'Форма бронирования'),
        ('call_to_action', 'Призыв к действию'),
        ('sections', 'Секции'),
        ('about', 'Об услуге'),
        ('features', 'Возможности'),
        ('partners', 'Партнёры'),
        ('form', 'Форма регистрации пользователя'),
        ('seo', 'SEO')
    )
