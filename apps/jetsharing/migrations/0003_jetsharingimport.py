# Generated by Django 3.1.3 on 2020-11-23 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jetsharing', '0002_auto_20201122_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='JetSharingImport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('import_file', models.FileField(upload_to='integrations/jet_sharing/%Y/%m/%d', verbose_name='XLS-файл')),
                ('is_processed', models.BooleanField(default=False, verbose_name='Обработано')),
                ('report', models.TextField(blank=True, null=True, verbose_name='Ошибки')),
            ],
            options={
                'verbose_name': 'Заявка на импорт рейсов JetSharing',
                'verbose_name_plural': 'Импорт рейсов JetSharing',
                'ordering': ('-created',),
            },
        ),
    ]
