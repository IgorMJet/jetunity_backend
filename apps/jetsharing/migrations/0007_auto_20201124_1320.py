# Generated by Django 3.0.11 on 2020-11-24 10:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jetsharing', '0006_jetsharingflight_plane_model'),
    ]

    operations = [
        migrations.AddField(
            model_name='jetsharingpage',
            name='booking_form_title',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Форма бронирования: заголовок формы'),
        ),
        migrations.AddField(
            model_name='jetsharingpage',
            name='booking_form_title_en',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Форма бронирования: заголовок формы'),
        ),
        migrations.AddField(
            model_name='jetsharingpage',
            name='booking_form_title_ru',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Форма бронирования: заголовок формы'),
        ),
    ]
