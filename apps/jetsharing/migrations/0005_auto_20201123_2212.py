# Generated by Django 3.0.11 on 2020-11-23 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jetsharing', '0004_auto_20201123_1947'),
    ]

    operations = [
        migrations.AddField(
            model_name='jetsharingflight',
            name='form_price_hint',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
        migrations.AddField(
            model_name='jetsharingflight',
            name='form_price_hint_en',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
        migrations.AddField(
            model_name='jetsharingflight',
            name='form_price_hint_ru',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
        migrations.AddField(
            model_name='jetsharingpage',
            name='form_price_hint',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
        migrations.AddField(
            model_name='jetsharingpage',
            name='form_price_hint_en',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
        migrations.AddField(
            model_name='jetsharingpage',
            name='form_price_hint_ru',
            field=models.TextField(blank=True, null=True, verbose_name='Подсказка для цены в списке рейсов'),
        ),
    ]
