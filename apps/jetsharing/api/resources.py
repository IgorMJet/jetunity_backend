from jetsharing import models
from jetsharing.api import serializers
from snippets.api.views import PublicViewMixin, PageRetrieveAPIView, \
    BaseListAPIView


class JetSharingPageView(PublicViewMixin, PageRetrieveAPIView):
    """Страница JetSharing"""

    queryset = models.JetSharingPage.objects.all()
    serializer_class = serializers.JetSharingPageSerializer


class JetSharingFlightsListView(PublicViewMixin, BaseListAPIView):
    """Список рейсов JetSharing"""

    queryset = models.JetSharingFlight.objects.published()
    serializer_class = serializers.JetSharingFlightSerializer

    def filter_queryset(self, queryset):
        qs = super(JetSharingFlightsListView, self).filter_queryset(queryset)
        serializer = serializers.JetSharingFlightFiltersSerializer(
            self.request.query_params
        )
        serializer.is_valid(raise_exception=True)
        airport_origin = serializer.validated_data.get('airport_origin')
        airport_destination = serializer.validated_data.get(
            'airport_destination'
        )
        origin_date = serializer.validated_data.get('origin_date')

        if airport_origin:
            qs = qs.filter(airport_origin=airport_origin)

        if airport_destination:
            qs = qs.filter(airport_destination=airport_destination)

        if origin_date:
            qs = qs.filter(origin_datetime__date=origin_date)

        return qs
