from django.urls import path

from jetsharing.api import resources


app_name = 'jetsharing'

urlpatterns = (
    path(
        'page/',
        resources.JetSharingPageView.as_view(),
        name='jetsharing_page'
    ),
    path(
        'flights/',
        resources.JetSharingFlightsListView.as_view(),
        name='jetsharing_flights'
    )
)
