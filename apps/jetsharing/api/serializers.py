from django.utils import timezone
from rest_framework import serializers

from handbooks.api.serializers import AirportSerializer
from handbooks.models import Airport
from jetsharing import models
from snippets.api.serializers import fields
from snippets.api.serializers.fields import SlugRelatedToPkField
from snippets.api.serializers.mixins import SEOSerializerMixin


class JetSharingFeatureSerializer(serializers.ModelSerializer):
    """Возможности JetSharing"""

    image = fields.FileField()

    class Meta:
        fields = ('id', 'image', 'title')
        model = models.JetSharingFeature


class JetSharingFlightSerializer(serializers.ModelSerializer):
    """Рейсы JetSharing"""

    airport_origin = AirportSerializer()
    airport_destination = AirportSerializer()
    form_price_hint = fields.PretextField()
    price = serializers.FloatField()

    class Meta:
        fields = (
            'airport_origin', 'airport_destination', 'destination_datetime',
            'flight_duration', 'flight_type', 'form_price_hint', 'free_seats',
            'id', 'origin_datetime', 'plane_model', 'price', 'total_seats'
        )
        model = models.JetSharingFlight


class JetSharingFlightFiltersSerializer(serializers.ModelSerializer):
    """Фильтры поиска рейсов"""

    airport_origin = SlugRelatedToPkField(
        queryset=Airport.objects.published(), required=False,
        allow_null=True, slug_field='id'
    )
    airport_destination = SlugRelatedToPkField(
        queryset=Airport.objects.published(), required=False,
        allow_null=True, slug_field='id'
    )
    origin_date = serializers.DateField()

    class Meta:
        fields = ('airport_origin', 'airport_destination', 'origin_date')
        model = models.JetSharingFlight


class JetSharingPartnerSerializer(serializers.ModelSerializer):
    """Партнеры JetSharing"""

    image = fields.ImageField()

    class Meta:
        fields = ('id', 'image', 'title')
        model = models.JetSharingPartner


class JetSharingSectionSerializer(serializers.ModelSerializer):
    """Секции JetSharing"""

    class Meta:
        fields = ('button_caption', 'button_url', 'content', 'id', 'title')
        model = models.JetSharingSection


class JetSharingPageSerializer(SEOSerializerMixin,
                               serializers.ModelSerializer):
    """Страница JetSharing"""

    about_image = fields.ImageField()
    features = fields.PublishedRelationField(
        JetSharingFeatureSerializer, many=True
    )
    flights = serializers.SerializerMethodField()
    form_bg = fields.ImageField()
    form_bg_2x = fields.ImageField()
    form_price_hint = fields.PretextField()
    form_sent_to_email = fields.PretextField()
    hero_image = fields.ImageField()
    meta = serializers.SerializerMethodField()
    partners = fields.PublishedRelationField(
        JetSharingPartnerSerializer, many=True
    )
    sections = fields.PublishedRelationField(
        JetSharingSectionSerializer, many=True
    )

    class Meta:
        fields = (
            'about_content', 'about_image', 'about_slogan', 'about_title',
            'booking_form_title', 'call_to_action_button_caption',
            'call_to_action_title', 'features', 'flights',
            'flights_not_found_text', 'form_bg', 'form_bg_2x',
            'form_copy_button', 'form_price_hint', 'form_sent_to_email',
            'form_share_button', 'form_subtitle', 'form_success_message_title',
            'form_success_message_content', 'form_title', 'hero_image',
            'hero_left_indicator_text', 'hero_left_indicator_value',
            'hero_right_indicator_text', 'hero_title', 'meta', 'partners',
            'partners_title', 'sections', 'sections_title', 'show_about',
            'show_call_to_action', 'show_form', 'show_partners',
            'show_sections', 'title'
        )
        model = models.JetSharingPage

    @staticmethod
    def get_flights(obj):
        qs = models.JetSharingFlight.objects.published().filter(
            origin_datetime__gte=timezone.now()
        ).filter(
            free_seats__gte=1
        ).order_by('ordering', 'origin_datetime')

        return JetSharingFlightSerializer(qs, many=True).data
