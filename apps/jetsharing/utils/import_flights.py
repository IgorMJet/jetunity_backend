import datetime
from decimal import Decimal

from django.contrib import messages
from django.db import transaction
import xlrd
from pytz import UTC

from handbooks.models import Airport
from jetsharing.choices import FlightTypeChoices
from jetsharing.models import JetSharingFlight


ORIGIN_DATETIME = 0
ORIGIN_AIRPORT_CODE = 1
DESTINATION_DATETIME = 2
DESTINATION_AIRPORT_CODE = 3
DURATION = 4
PRICE = 5
FLIGHT_TYPE = 6
PLANE_MODEL = 7
TOTAL_SEATS = 8
FREE_SEATS = 9


def parse_datetime(dt):
    if isinstance(dt, str):
        dt = str(dt).strip().lower().replace('/', '.')
        try:
            date, time = dt.split(' ')
        except ValueError:
            a = 1
        day, month, year = date.split('.')
        hour, minute = time.split(':')
        return datetime.datetime(
            int('20' + year), int(month), int(day), int(hour), int(minute),
            tzinfo=UTC
        )

    start = datetime.datetime(1899, 12, 30)
    total_days = int(dt)
    result_date = start + datetime.timedelta(days=total_days)
    if dt > total_days:
        time = parse_duration(dt - total_days)
        result_date = result_date.replace(hour=time.hour, minute=time.minute)

    return result_date


def parse_duration(value):
    if isinstance(value, (float, int, Decimal)):
        hours = 24.0 * float(value)
        hour = int(hours)
        minute = int((hours - hour) * 60.0)
        if minute == 59:
            minute = 0
            hour += 1
        return datetime.time(hour, minute, 0)

    if 'ч' in value:
        hour = int(value.replace('ч', ''))
        return datetime.time(hour, 0, 0)

    hour, minute, second = value.split(':')
    return datetime.time(int(hour), int(minute), int(second))


def import_jetsharing_flights(request_object, request):
    if not request_object.id or request_object.is_processed:
        return False

    report = []
    rows_processed = 0

    with transaction.atomic():
        JetSharingFlight.objects.all().delete()
        rb = xlrd.open_workbook(request_object.import_file.path)
        sheet = rb.sheet_by_index(0)
        for rownum in range(sheet.nrows):
            if rownum == 0:
                continue

            row = sheet.row_values(rownum)
            rows_processed += 1
            print(f'Uploading {rownum}')
            if len(row) < 10:
                continue

            origin_dt = parse_datetime(row[ORIGIN_DATETIME])
            origin_airport_code = str(row[ORIGIN_AIRPORT_CODE]).strip()
            destination_dt = parse_datetime(row[DESTINATION_DATETIME])
            destination_airport_code = str(
                row[DESTINATION_AIRPORT_CODE]
            ).strip()
            duration = parse_duration(row[DURATION]) if row[DURATION] else None
            price = Decimal(row[PRICE]) if row[PRICE] else Decimal('0')
            flight_type = row[FLIGHT_TYPE] if row[FLIGHT_TYPE] else None
            plane_model = row[PLANE_MODEL] or None
            total_seats = int(row[TOTAL_SEATS])
            free_seats = int(row[FREE_SEATS])

            origin_airport = Airport.objects.get_or_create(
                code=origin_airport_code
            )[0]
            destination_airport = Airport.objects.get_or_create(
                code=destination_airport_code
            )[0]

            if flight_type:
                flight_type = flight_type.lower()
                if flight_type == 'sharing':
                    flight_type = FlightTypeChoices.SHARING.value
                else:
                    flight_type = FlightTypeChoices.SHUTTLE.value

            JetSharingFlight.objects.create(
                airport_origin=origin_airport,
                airport_destination=destination_airport,
                origin_datetime=origin_dt,
                destination_datetime=destination_dt,
                flight_duration=duration,
                plane_model=plane_model,
                total_seats=total_seats,
                free_seats=free_seats,
                price=price,
                flight_type=flight_type
            )

    request_object.report = '\n'.join(report) if report else None
    if not report:
        request_object.is_processed = True
        messages.success(request, 'Строк обработано: %s' % rows_processed)
    else:
        print('!!!Errors:')
        for row in report:
            print(row)

        messages.warning(
            request, 'Процесс прошел частично. Посмотрите лог ошибок'
        )
    request_object.save()
