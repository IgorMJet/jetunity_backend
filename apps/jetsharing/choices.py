from django.db.models import TextChoices
from django.utils.translation import ugettext_lazy as _


class FlightTypeChoices(TextChoices):
    """Типы рейсов"""

    SHUTTLE = 'shuttle', _('Shuttle')
    SHARING = 'sharing', _('Sharing')
