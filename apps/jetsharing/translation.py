from modeltranslation.decorators import register

from jetsharing import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.JetSharingFeature)
class JetSharingFeatureTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingFeature.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingFlight)
class JetSharingFlightTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingFlight.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingPage)
class JetSharingPageTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingPage.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingPartner)
class JetSharingPartnerTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingPartner.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingSection)
class JetSharingSectionTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingSection.translation_fields
    required_languages = {'default': ()}
