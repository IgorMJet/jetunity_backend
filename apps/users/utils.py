import hashlib

from django.db import ProgrammingError

from snippets.utils.passwords import generate_alt_id


def get_user_auth_token_subject(user):
    salt_hash = hashlib.sha256(user.token_salt.encode('utf-8')).hexdigest()
    return ''.join([x for i, x in enumerate(salt_hash) if i % 4 == 0])


def generate_referral_code():
    from users.models import User
    code = generate_alt_id(length=6).lower()
    try:
        while User.objects.filter(referral_code=code).exists():
            code = generate_alt_id(length=6).lower()
    except ProgrammingError:
        return code
    return code
