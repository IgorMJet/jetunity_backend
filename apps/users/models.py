from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from snippets.models import LastModMixin, BasicModel
from users.managers import UserManager
from users.utils import generate_referral_code


class User(AbstractUser, LastModMixin, BasicModel):
    """Пользователь"""

    REQUIRED_FIELDS = ['email']

    username = models.CharField(
        _('username'),
        max_length=254,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ '
            'only.'
        ),
        validators=[AbstractUser.username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email'), blank=True, null=True)
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True, null=True)
    phone_number = models.CharField(
        'Телефон', max_length=50, blank=True, null=True
    )
    is_customer = models.BooleanField('Клиент', default=False)
    is_broker = models.BooleanField('Брокер', default=False)
    is_partner = models.BooleanField('Партнер', default=False)
    referral_code = models.CharField(
        'Реферральный код', max_length=6, default=generate_referral_code,
        unique=True
    )

    email_verified_date = models.DateTimeField(
        'Дата и время подтверждения email', blank=True, null=True
    )
    restore_salt = models.CharField(
        'Соль восстановления пароля', max_length=80,
        help_text='Выставляется при запросе восстановления пароля и удаляется '
                  'после успешной смены пароля, либо по сроку годности',
        blank=True, null=True
    )
    restore_salt_expiry = models.DateTimeField(
        'Срок годности соли восстановления', blank=True, null=True
    )
    restore_token = models.CharField(
        'Токен восстановления пароля', max_length=80,
        help_text='Выставляется при подтверждении email\'а запроса '
                  'восстановления пароля. Удаляется после успешной смены '
                  'пароля, либо по сроку годности',
        blank=True, null=True
    )
    restore_token_expiry = models.DateTimeField(
        'Срок годности токена восстановления', blank=True, null=True
    )

    email_fields = ('email', 'first_name', 'last_name', 'username')

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    objects = UserManager()

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        parts = map(
            lambda x: x.strip(),
            filter(None, (self.last_name, self.first_name))
        )
        full_name = ' '.join(parts)

        return full_name or self.username

    get_full_name.short_description = 'ФИО'
    get_full_name.admin_order_field = 'full_name'

    @property
    def user_types(self):
        types = []
        if self.is_customer:
            types.append('customer')
        if self.is_broker:
            types.append('broker')
        if self.is_partner:
            types.append('partner')

        return types
