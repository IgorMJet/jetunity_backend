from import_export import resources

from users import models


class UserResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'first_name', 'last_name', 'email', 'phone_number', 'is_active', 'is_customer',
            'is_broker', 'is_partner', 'referral_code',  'date_joined', 'is_staff', 'is_superuser'
        )
        export_order = fields[:]
        model = models.User
        skip_unchanged = True
