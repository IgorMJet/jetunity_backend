from django.contrib import admin

from modeltranslation.admin import TranslationAdmin
from solo.admin import SingletonModelAdmin

from referrals import models


@admin.register(models.ReferralProgramPage)
class ReferralProgramPageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Страница реферральной программы"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': (
                'hero_title', 'hero_link_caption', 'hero_link_url', 'hero_content',
                'hero_footnote', 'hero_button_caption', 'hero_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-about'),
            'fields': (
                'show_about', 'about_title', 'about_content', 'about_button_caption',
                'about_button_url', 'show_about_gallery', 'about_gallery'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-form'),
            'fields': (
                'form_title', 'form_subtitle', 'form_bg', 'form_bg_2x',
                'form_success_message_title', 'form_success_message_content', 'form_share_button',
                'form_copy_button', 'form_sent_to_email'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    raw_id_fields = ('about_gallery',)
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('about', 'О программе'),
        ('form', 'Форма'),
        ('seo', 'SEO')
    )
