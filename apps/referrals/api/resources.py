from referrals import models
from referrals.api import serializers
from snippets.api.views import PublicViewMixin, PageRetrieveAPIView


class ReferralProgramPageView(PublicViewMixin, PageRetrieveAPIView):
    """Главная страница"""

    queryset = models.ReferralProgramPage.objects.all()
    serializer_class = serializers.ReferralProgramPageSerializer
