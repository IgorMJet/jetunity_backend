from rest_framework import serializers

from core.api.serializers import GallerySerializer
from referrals import models
from snippets.api.serializers import fields
from snippets.api.serializers.mixins import SEOSerializerMixin


class ReferralProgramPageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Слайды на главной"""

    about_gallery = fields.PublishedRelationField(GallerySerializer, many=False)
    form_bg = fields.ImageField()
    form_bg_2x = fields.ImageField()
    form_sent_to_email = fields.PretextField()
    hero_image = fields.ImageField()
    meta = serializers.SerializerMethodField()

    class Meta:
        fields = (
            'about_button_caption', 'about_button_url', 'about_content', 'about_gallery',
            'about_title', 'form_bg', 'form_bg_2x', 'form_copy_button', 'form_sent_to_email',
            'form_share_button', 'form_subtitle', 'form_success_message_title',
            'form_success_message_content', 'form_title', 'hero_button_caption', 'hero_content',
            'hero_footnote', 'hero_image', 'hero_link_caption', 'hero_link_url', 'hero_title',
            'meta', 'show_about', 'show_about_gallery', 'title'
        )
        model = models.ReferralProgramPage
