from django.urls import path

from referrals.api import resources


app_name = 'referrals'

urlpatterns = (
    path(
        'page/',
        resources.ReferralProgramPageView.as_view(),
        name='referrals_page'
    ),
)
