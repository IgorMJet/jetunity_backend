from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'referrals'
    verbose_name = 'Реферральная программа'
