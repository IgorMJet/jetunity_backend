from modeltranslation.decorators import register

from referrals import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.ReferralProgramPage)
class ReferralProgramPageTranslationOptions(BaseTranslationOptions):
    fields = models.ReferralProgramPage.translation_fields
    required_languages = {'default': ()}
