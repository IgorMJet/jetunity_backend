from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField

from core.models import ReferralCodeFormPageMixin
from snippets.models.pages import BasePage


class ReferralProgramPage(ReferralCodeFormPageMixin, BasePage):
    """Страница реферральной программы"""

    hero_title = models.CharField('Первый экран: заголовок', max_length=255, blank=True, null=True)
    hero_link_caption = models.CharField(
        'Первый экран: текст ссылки в заголовке', max_length=255, blank=True, null=True
    )
    hero_link_url = models.CharField(
        'Первый экран: ссылка в заголовке', max_length=255, blank=True, null=True
    )
    hero_content = RichTextUploadingField('Первый экран: список', blank=True, null=True)
    hero_footnote = models.CharField('Первый экран: сноска', max_length=255, blank=True, null=True)
    hero_button_caption = models.CharField(
        'Первый экран: текст кнопки', max_length=255, blank=True, null=True
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='referrals', blank=True, null=True
    )
    show_about = models.BooleanField('Показывать блок О программе', default=True)
    about_title = models.CharField(
        'О программе: заголовок', max_length=255, blank=True, null=True
    )
    about_content = RichTextUploadingField('О программе: контент', blank=True, null=True)
    about_button_caption = models.CharField(
        'О программе: текст кнопки', max_length=255, blank=True, null=True
    )
    about_button_url = models.CharField(
        'О программе: ссылка кнопки', max_length=255, blank=True, null=True
    )
    show_about_gallery = models.BooleanField('О программе: показывать галерею', default=True)
    about_gallery = models.ForeignKey(
        'core.Gallery', verbose_name='О программе: галерея', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    form_title = models.CharField('Форма: заголовок', max_length=255, blank=True, null=True)
    form_subtitle = models.CharField('Форма: подзаголовок', max_length=255, blank=True, null=True)
    form_bg = models.ImageField(
        'Форма: фон', max_length=255, upload_to='referrals/bgs', blank=True, null=True
    )
    form_bg_2x = models.ImageField(
        'Форма: фон (двойное качество)', max_length=255, upload_to='referrals/bgs', blank=True,
        null=True
    )

    translation_fields = BasePage.translation_fields + \
        ReferralCodeFormPageMixin.translation_fields + (
            'about_button_caption', 'about_button_url', 'about_content', 'about_title',
            'form_subtitle', 'form_title', 'hero_button_caption', 'hero_content', 'hero_footnote',
            'hero_link_caption', 'hero_link_url', 'hero_title',
        )

    class Meta:
        verbose_name = 'Страница реферральной программы'
        verbose_name_plural = 'Страница реферральной программы'

    def __str__(self):
        return 'Страница реферральной программы'
