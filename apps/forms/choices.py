from django.db import models


class FormRequestReadStatusChoices(models.IntegerChoices):
    """Read status choices"""

    UNREAD = -1, 'Не прочитано'
    READ = 1, 'Прочитано'
