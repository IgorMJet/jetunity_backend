from django.contrib import admin
from django.contrib.admin.exceptions import DisallowedModelAdminToField
from django.contrib.admin.options import TO_FIELD_VAR
from django.contrib.admin.utils import unquote

from import_export.admin import ExportMixin
from import_export.formats import base_formats

from forms import models, import_export
from forms.choices import FormRequestReadStatusChoices


class BaseFormRequestAdmin(ExportMixin, admin.ModelAdmin):
    """Base class for form request"""

    date_hierarchy = 'created'
    formats = [
        base_formats.CSV, base_formats.XLS, base_formats.HTML,
        base_formats.ODS, base_formats.TSV
    ]
    list_display = ('id', 'language', 'updated')
    list_filter = (
        'read_status', 'language', 'utm_source', 'utm_medium', 'utm_campaign'
    )
    ordering = ('-created',)
    raw_id_fields = ('user', 'referral_user')
    readonly_fields = (
        'created', 'updated', 'utm_campaign', 'utm_content', 'utm_medium',
        'utm_source', 'utm_term'
    )
    search_fields = (
        '=id', 'user__first_name', 'user__last_name', 'user__phone_number',
        'user__email', 'referral_user__first_name', 'referral_user__last_name',
        'referral_user__phone_number', 'referral_user__email', 'utm_campaign',
        'utm_content', 'utm_medium', 'utm_source', 'utm_term'
    )

    @staticmethod
    def suit_row_attributes(obj, request):
        return {'class': 'is-unread' if obj.read_status < 0 else 'is-read'}

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """Marks object read"""
        if object_id:

            to_field = request.POST.get(TO_FIELD_VAR, request.GET.get(TO_FIELD_VAR))
            if to_field and not self.to_field_allowed(request, to_field):
                raise DisallowedModelAdminToField("The field %s cannot be referenced." % to_field)

            obj = self.get_object(request, unquote(object_id), to_field)
            if obj and obj.read_status < 0:
                obj.read_status = FormRequestReadStatusChoices.READ.value
                obj.save()

        return super(BaseFormRequestAdmin, self).change_view(
            request, object_id, form_url=form_url, extra_context=extra_context
        )


class BookingFlightInline(admin.TabularInline):
    """Рейсы бронирования"""

    extra = 0
    fields = models.BookingFlight().collect_fields()
    model = models.BookingFlight
    readonly_fields = ('created', 'updated')


@admin.register(models.Booking)
class BookingAdmin(BaseFormRequestAdmin):
    """Бронирования"""

    fields = models.Booking().collect_fields()
    inlines = (BookingFlightInline,)
    list_display = (
        'booking_code', 'first_name', 'last_name', 'phone_number', 'email',
        'referral_user', 'created'
    )
    list_display_links = (
        'booking_code', 'first_name', 'last_name', 'phone_number'
    )
    list_select_related = True
    readonly_fields = BaseFormRequestAdmin.readonly_fields + ('booking_code',)
    resource_class = import_export.BookingResource
    search_fields = BaseFormRequestAdmin.search_fields + (
        'email', 'first_name', 'last_name', 'phone_number',
        'flights__origin_text', 'flights__destination_text', 'booking_code',
        'referral_user__username', 'referral_user__first_name',
        'referral_user__last_name', 'referral_user__phone_number'
    )


@admin.register(models.BrokerRequest)
class BrokerRequestAdmin(BaseFormRequestAdmin):
    """Заявки брокеров"""

    fields = models.BrokerRequest().collect_fields()
    list_display = (
        'id', 'name', 'phone_number', 'email', 'referral_user', 'url',
        'created'
    )
    list_display_links = ('id', 'name', 'phone_number')
    list_filter = BaseFormRequestAdmin.list_filter + ('url',)
    list_select_related = True
    resource_class = import_export.BrokerRequestResource
    search_fields = BaseFormRequestAdmin.search_fields + (
        'email', 'name', 'phone_number', 'url', 'referral_user__username',
        'referral_user__first_name', 'referral_user__last_name',
        'referral_user__phone_number'
    )


class JetSharingBookingFlightInline(admin.TabularInline):
    """Рейсы бронирования JetSharing"""

    extra = 0
    fields = models.JetSharingBookingFlight().collect_fields()
    model = models.JetSharingBookingFlight
    raw_id_fields = ('airport_origin', 'airport_destination')
    readonly_fields = ('created', 'updated')


@admin.register(models.JetSharingBooking)
class JetSharingBookingAdmin(BaseFormRequestAdmin):
    """Бронирования JetSharing"""

    fields = models.JetSharingBooking().collect_fields()
    inlines = (JetSharingBookingFlightInline,)
    list_display = (
        'booking_code', 'first_name', 'last_name', 'phone_number', 'email',
        'seats', 'referral_user', 'created'
    )
    list_display_links = ('booking_code', 'first_name', 'last_name')
    list_filter = (
        'flights__airport_origin',
        'flights__airport_destination',
        'want_return_flight'
    ) + BaseFormRequestAdmin.list_filter
    list_select_related = True
    readonly_fields = BaseFormRequestAdmin.readonly_fields + ('booking_code',)
    resource_class = import_export.JetSharingBookingResource
    search_fields = BaseFormRequestAdmin.search_fields + (
        'email', 'first_name', 'last_name', 'phone_number', 'seats',
        'flights__airport_origin__title',
        'flights__airport_origin__code',
        'flights__airport_origin__city__title',
        'flights__airport_destination__title',
        'flights__airport_destination__code',
        'flights__airport_destination__city__title', 'booking_code',
        'referral_user__username', 'referral_user__first_name',
        'referral_user__last_name', 'referral_user__phone_number'
    )


@admin.register(models.PartnershipRequest)
class PartnershipRequestAdmin(BaseFormRequestAdmin):
    """Заявки на сотрудничество"""

    fields = models.PartnershipRequest().collect_fields()
    list_display = (
        'id', 'name', 'phone_number', 'email', 'referral_user', 'url',
        'created'
    )
    list_display_links = ('id', 'name', 'phone_number')
    list_filter = BaseFormRequestAdmin.list_filter + ('url',)
    list_select_related = True
    resource_class = import_export.PartnershipRequestResource
    search_fields = BaseFormRequestAdmin.search_fields + (
        'email', 'name', 'phone_number', 'url', 'referral_user__username',
        'referral_user__first_name', 'referral_user__last_name',
        'referral_user__phone_number'
    )


@admin.register(models.UserRequest)
class UserRequestAdmin(BaseFormRequestAdmin):
    """Запросы на регистрацию пользователей"""

    fields = models.UserRequest().collect_fields()
    list_display = (
        'id', 'name', 'phone_number', 'email', 'referral_user', 'url',
        'created'
    )
    list_display_links = ('id', 'name', 'phone_number')
    list_filter = BaseFormRequestAdmin.list_filter + ('url',)
    list_select_related = True
    resource_class = import_export.UserRequestResource
    search_fields = BaseFormRequestAdmin.search_fields + (
        'email', 'name', 'phone_number', 'url', 'referral_user__username',
        'referral_user__first_name', 'referral_user__last_name',
        'referral_user__phone_number'
    )
