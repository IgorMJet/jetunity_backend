from import_export import resources

from forms import models


class BookingResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'first_name', 'last_name', 'phone_number', 'email', 'url',
            'user_id', 'referral_user_id', 'created', 'utm_campaign',
            'utm_content', 'utm_medium', 'utm_source', 'utm_term'
        )
        export_order = fields[:]
        model = models.Booking
        skip_unchanged = True


class BrokerRequestResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'name', 'phone_number', 'email', 'url', 'user_id',
            'referral_user_id', 'created', 'utm_campaign', 'utm_content',
            'utm_medium', 'utm_source', 'utm_term'
        )
        export_order = fields[:]
        model = models.BrokerRequest
        skip_unchanged = True


class JetSharingBookingResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'first_name', 'last_name', 'phone_number', 'email', 'seats',
            'want_return_flight', 'url', 'user_id', 'referral_user_id',
            'created', 'utm_campaign', 'utm_content', 'utm_medium',
            'utm_source', 'utm_term'
        )
        export_order = fields[:]
        model = models.JetSharingBooking
        skip_unchanged = True


class PartnershipRequestResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'name', 'phone_number', 'email', 'url', 'user_id',
            'referral_user_id', 'created', 'utm_campaign', 'utm_content',
            'utm_medium', 'utm_source', 'utm_term'
        )
        export_order = fields[:]
        model = models.PartnershipRequest
        skip_unchanged = True


class UserRequestResource(resources.ModelResource):
    class Meta:
        fields = (
            'id', 'name', 'phone_number', 'email', 'url', 'user_id',
            'referral_user_id', 'created', 'utm_campaign', 'utm_content',
            'utm_medium', 'utm_source', 'utm_term'
        )
        export_order = fields[:]
        model = models.UserRequest
        skip_unchanged = True
