from django.conf import settings
from django.db import ProgrammingError


def generate_booking_code():
    from forms.models import Booking

    prefix = 'JU-' if settings.SITE_NAME.startswith('jetunity') else 'ST-'
    last_booking_number = 7000

    try:
        last_booking = Booking.objects.order_by('-created').first()
        if last_booking and last_booking.booking_code.startswith(prefix):
            last_booking_number = int(last_booking.booking_code[len(prefix):])
    except ProgrammingError:
        pass

    return f'{prefix}{last_booking_number + 1}'


def generate_jetsharing_booking_code():
    from forms.models import JetSharingBooking

    prefix = 'JS-'
    last_booking_number = 7000

    try:
        last_booking = JetSharingBooking.objects.order_by('-created').first()
        if last_booking and last_booking.booking_code.startswith(prefix):
            last_booking_number = int(last_booking.booking_code[len(prefix):])
    except ProgrammingError:
        pass

    return f'{prefix}{last_booking_number + 1}'
