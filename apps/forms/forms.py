from django import forms

from forms import models


class BaseRequestForm(forms.ModelForm):
    """Base request forms class"""
    class Meta:
        fields = '__all__'
        exclude = ('read_status',)


class BrokerRequestForm(BaseRequestForm):
    class Meta:
        fields = '__all__'
        exclude = ('read_status',)
        model = models.BrokerRequest


class PartnershipRequestForm(BaseRequestForm):
    class Meta:
        fields = '__all__'
        exclude = ('read_status',)
        model = models.PartnershipRequest


class UserRequestForm(BaseRequestForm):
    class Meta:
        fields = '__all__'
        exclude = ('read_status',)
        model = models.UserRequest
