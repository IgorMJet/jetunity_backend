from django.urls import path

from forms.api import resources


app_name = 'forms'

urlpatterns = (
    path(
        'booking/',
        resources.BookingView.as_view(),
        name='booking'
    ),
    path(
        'brokers/',
        resources.BrokerRequestView.as_view(),
        name='brokers_request'
    ),
    path(
        'jetsharing/booking/',
        resources.JetSharingBookingView.as_view(),
        name='jetsharing_booking'
    ),
    path(
        'partnership/',
        resources.PartnershipRequestView.as_view(),
        name='partnership_request'
    ),
    path(
        'user/',
        resources.UserRequestView.as_view(),
        name='user_request'
    )
)
