from copy import deepcopy

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from drf_yasg.utils import swagger_auto_schema
from raven.contrib.django.raven_compat.models import client as raven_client
import requests
from rest_framework.views import APIView

from forms.api import serializers
from forms import models
from snippets.api.swagger import openapi_responses
from snippets.api.views import PublicViewMixin
from snippets.http.response import success_response, validation_error_response
from snippets.template_backends.jinja2.globals import site_url
from snippets.utils.email import send_trigger_email, get_admin_emails,\
    send_email
from vars.models import BookingEmailSettings, PartnerEmailSettings, \
    UserEmailSettings, JetSharingBookingEmailSettings


class BaseFormRequestView(PublicViewMixin, APIView):
    """Базовый класс запросов клиентов """

    http_method_names = ['post']
    success_message = _('Спасибо, ваш запрос отправлен!')
    serializer_class = None

    def create(self, serializer):
        obj = serializer.save()
        return obj

    @staticmethod
    def get_admin_emails(request):
        return get_admin_emails(request=request)

    def get_event_name(self, request, obj):
        return 'новая отправка формы "%s"' % obj._meta.verbose_name

    def get_success_message(self, request, obj):
        return self.success_message

    @staticmethod
    def send_postback(form_name, data):
        json_data = deepcopy(data)
        json_data.update({
            'form_name': form_name,
            'domain': settings.SITE_NAME
        })
        print(json_data)

        try:
            r = requests.post(
                # 'https://webhook.site/3301ee0e-16a3-4b4d-a13b-ef62d8bc557d',
                'https://link.gurucrm.ru/api/hooks/queue/5c6e9da4fb_6c083abd',
                json=json_data,
                verify=False
            )
            print(r.status_code, r.text)

        except requests.RequestException:
            print('postback failed')
            raven_client.captureException()

    def success_trigger(self, request, obj):
        pass

    def post(self, request, lang, **kwargs):
        data = deepcopy(request.data)
        data['language'] = lang
        serializer = self.serializer_class(data=data, context={
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        })

        if not serializer.is_valid():
            return validation_error_response(serializer.errors)

        obj = self.create(serializer)

        event = self.get_event_name(request, obj)
        send_trigger_email(
            event, request=request, obj=obj, fields=obj.email_fields,
            emails=self.get_admin_emails(request)
        )

        self.success_trigger(request, obj)

        return success_response(self.get_success_message(request, obj))


class BookingView(BaseFormRequestView):
    """Форма бронирования"""

    serializer_class = serializers.BookingSerializer
    success_message = _('Спасибо, ваша заявка отправлена!')

    def create(self, serializer):
        # бронь
        obj = serializer.save()

        # рейсы
        flights = self.request.data.get('flights', [])
        for flight_data in flights:
            flight_serializer = serializers.BookingFlightSerializer(
                data=flight_data, context={
                    'request': self.request,
                    'format': self.format_kwarg,
                    'view': self
                }
            )
            flight_serializer.is_valid(raise_exception=True)
            flight = models.BookingFlight(
                booking=obj, **flight_serializer.validated_data
            )
            flight.save()
        return obj

    def get_event_name(self, request, obj):
        return _('Бронирование')

    def get_success_message(self, request, obj):
        return {
            'message': self.success_message,
            'referral_code': obj.user.referral_code if obj.user_id else None
        }

    def success_trigger(self, request, obj):
        self.send_postback('booking', {
            'id': obj.id,
            'flights': [{
                'id': flight.id,
                'origin': flight.origin_text,
                'destination': flight.destination_text,
                'flight_date': flight.flight_date.isoformat()
                if flight.flight_date else None,
            } for flight in obj.flights.all()],
            'first_name': obj.first_name,
            'last_name': obj.last_name,
            'phone_number': obj.phone_number,
            'email': obj.email,
            'booking_code': obj.booking_code,
            'referral_user_id': obj.referral_user_id,
            'referral_user_code': obj.referral_user.referral_code
            if obj.referral_user_id else None,
            'referral_user_full_name': obj.referral_user.get_full_name()
            if obj.referral_user_id else None,
            'user_id': obj.user_id,
            'user_referral_code': obj.user.referral_code
            if obj.user_id else None,
            'user_types': obj.user.user_types if obj.user_id else None,
            'created': obj.created.isoformat(),
            'utm_campaign': obj.utm_campaign or '',
            'utm_content': obj.utm_content or '',
            'utm_medium': obj.utm_medium or '',
            'utm_source': obj.utm_source or '',
            'utm_term': obj.utm_term or ''
        })

        if obj.email:
            conf = BookingEmailSettings.get_solo()
            send_email(
                'booking',
                [obj.email],
                f'{conf.subject} №{obj.booking_code}'
                if conf.subject else str(self.get_event_name(request, obj)),
                params={
                    'conf': conf,
                    'obj': obj,
                    'request': request,
                    'site_url': site_url()
                },
                raise_error=False
            )

    @swagger_auto_schema(
        request_body=serializer_class,
        responses={
            200: openapi_responses.success_response,
            400: openapi_responses.validation_error_response,
        }
    )
    def post(self, request, lang, **kwargs):
        return super(BookingView, self).post(request, lang, **kwargs)


class BrokerRequestView(BaseFormRequestView):
    """Форма заявки брокера"""

    serializer_class = serializers.BrokerRequestSerializer
    success_message = _('Спасибо, заявка отправлена!')

    def get_event_name(self, request, obj):
        return _('Заявка брокера')

    def get_success_message(self, request, obj):
        return {
            'message': self.success_message,
            'referral_code': obj.user.referral_code if obj.user_id else None
        }

    @swagger_auto_schema(
        request_body=serializer_class,
        responses={
            200: openapi_responses.success_response,
            400: openapi_responses.validation_error_response,
        }
    )
    def post(self, request, lang, **kwargs):
        return super(BrokerRequestView, self).post(request, lang, **kwargs)

    def success_trigger(self, request, obj):
        self.send_postback('broker', {
            'id': obj.id,
            'full_name': obj.name,
            'phone_number': obj.phone_number,
            'email': obj.email,
            'referral_user_id': obj.referral_user_id,
            'referral_user_code': obj.referral_user.referral_code
            if obj.referral_user_id else None,
            'referral_user_full_name': obj.referral_user.get_full_name()
            if obj.referral_user_id else None,
            'user_id': obj.user_id,
            'user_referral_code': obj.user.referral_code
            if obj.user_id else None,
            'user_types': obj.user.user_types if obj.user_id else None,
            'created': obj.created.isoformat(),
            'utm_campaign': obj.utm_campaign or '',
            'utm_content': obj.utm_content or '',
            'utm_medium': obj.utm_medium or '',
            'utm_source': obj.utm_source or '',
            'utm_term': obj.utm_term or ''
        })


class JetSharingBookingView(BaseFormRequestView):
    """Форма бронирования JetSharing"""

    serializer_class = serializers.JetSharingBookingSerializer
    success_message = _('Спасибо, ваша заявка отправлена!')

    def create(self, serializer):
        # бронь
        obj = serializer.save()

        # рейсы
        flights = self.request.data.get('flights', [])
        for flight_data in flights:
            flight_serializer = serializers.JetSharingBookingFlightSerializer(
                data=flight_data, context={
                    'request': self.request,
                    'format': self.format_kwarg,
                    'view': self
                }
            )
            flight_serializer.is_valid(raise_exception=True)
            flight = models.JetSharingBookingFlight(
                booking=obj, **flight_serializer.validated_data
            )
            flight.save()
        return obj

    def get_event_name(self, request, obj):
        return _('Бронирование JetSharing')

    def get_success_message(self, request, obj):
        return {
            'message': self.success_message,
            'referral_code': obj.user.referral_code if obj.user_id else None
        }

    def success_trigger(self, request, obj):
        self.send_postback('booking_jetsharing', {
            'id': obj.id,
            'flights': [{
                'id': flight.id,
                'origin': str(flight.airport_origin),
                'destination': str(flight.airport_destination),
                'flight_date': flight.origin_datetime.isoformat()
                if flight.origin_datetime else None,
            } for flight in obj.flights.select_related(
                'airport_origin',
                'airport_destination'
            )],
            'first_name': obj.first_name,
            'last_name': obj.last_name,
            'phone_number': obj.phone_number,
            'email': obj.email,
            'seats': obj.seats,
            'want_return_flight': obj.want_return_flight,
            'booking_code': obj.booking_code,
            'referral_user_id': obj.referral_user_id,
            'referral_user_code': obj.referral_user.referral_code
            if obj.referral_user_id else None,
            'referral_user_full_name': obj.referral_user.get_full_name()
            if obj.referral_user_id else None,
            'user_id': obj.user_id,
            'user_referral_code': obj.user.referral_code
            if obj.user_id else None,
            'user_types': obj.user.user_types if obj.user_id else None,
            'created': obj.created.isoformat(),
            'utm_campaign': obj.utm_campaign or '',
            'utm_content': obj.utm_content or '',
            'utm_medium': obj.utm_medium or '',
            'utm_source': obj.utm_source or '',
            'utm_term': obj.utm_term or ''
        })

        if obj.email:
            conf = JetSharingBookingEmailSettings.get_solo()
            send_email(
                'booking',
                [obj.email],
                f'{conf.subject} №{obj.booking_code}'
                if conf.subject else str(self.get_event_name(request, obj)),
                params={
                    'conf': conf,
                    'obj': obj,
                    'request': request,
                    'site_url': site_url()
                },
                raise_error=False
            )

    @swagger_auto_schema(
        request_body=serializer_class,
        responses={
            200: openapi_responses.success_response,
            400: openapi_responses.validation_error_response,
        }
    )
    def post(self, request, lang, **kwargs):
        return super(JetSharingBookingView, self).post(request, lang, **kwargs)


class PartnershipRequestView(BaseFormRequestView):
    """Форма заявки на сотрудничество"""

    serializer_class = serializers.PartnershipRequestSerializer
    success_message = _('Спасибо, заявка отправлена!')

    def get_event_name(self, request, obj):
        return _('Заявка на сотрудничество')

    def get_success_message(self, request, obj):
        return {
            'message': self.success_message,
            'referral_code': obj.user.referral_code if obj.user_id else None
        }

    def success_trigger(self, request, obj):
        self.send_postback('partner', {
            'id': obj.id,
            'full_name': obj.name,
            'phone_number': obj.phone_number,
            'email': obj.email,
            'referral_user_id': obj.referral_user_id,
            'referral_user_code': obj.referral_user.referral_code
            if obj.referral_user_id else None,
            'referral_user_full_name': obj.referral_user.get_full_name()
            if obj.referral_user_id else None,
            'user_id': obj.user_id,
            'user_referral_code': obj.user.referral_code
            if obj.user_id else None,
            'user_types': obj.user.user_types if obj.user_id else None,
            'created': obj.created.isoformat(),
            'utm_campaign': obj.utm_campaign or '',
            'utm_content': obj.utm_content or '',
            'utm_medium': obj.utm_medium or '',
            'utm_source': obj.utm_source or '',
            'utm_term': obj.utm_term or ''
        })

        if obj.email:
            conf = PartnerEmailSettings.get_solo()
            send_email(
                'partner',
                [obj.email],
                conf.subject or str(self.get_event_name(request, obj)),
                params={
                    'conf': conf,
                    'obj': obj,
                    'request': request,
                    'site_url': site_url()
                },
                raise_error=False
            )

    @swagger_auto_schema(
        request_body=serializer_class,
        responses={
            200: openapi_responses.success_response,
            400: openapi_responses.validation_error_response,
        }
    )
    def post(self, request, lang, **kwargs):
        return super(PartnershipRequestView, self).post(
            request, lang, **kwargs
        )


class UserRequestView(BaseFormRequestView):
    """Форма заявки на регистрацию пользователя"""

    serializer_class = serializers.UserRequestSerializer
    success_message = _('Спасибо, заявка отправлена!')

    def get_event_name(self, request, obj):
        return _('Заявка на регистрацию пользователя')

    def get_success_message(self, request, obj):
        return {
            'message': self.success_message,
            'referral_code': obj.user.referral_code if obj.user_id else None
        }

    def success_trigger(self, request, obj):
        self.send_postback('user', {
            'id': obj.id,
            'full_name': obj.name,
            'phone_number': obj.phone_number,
            'email': obj.email,
            'referral_user_id': obj.referral_user_id,
            'referral_user_code': obj.referral_user.referral_code
            if obj.referral_user_id else None,
            'referral_user_full_name': obj.referral_user.get_full_name()
            if obj.referral_user_id else None,
            'user_id': obj.user_id,
            'user_referral_code': obj.user.referral_code
            if obj.user_id else None,
            'user_types': obj.user.user_types if obj.user_id else None,
            'created': obj.created.isoformat(),
            'utm_campaign': obj.utm_campaign or '',
            'utm_content': obj.utm_content or '',
            'utm_medium': obj.utm_medium or '',
            'utm_source': obj.utm_source or '',
            'utm_term': obj.utm_term or ''
        })

        if obj.email:
            conf = UserEmailSettings.get_solo()
            send_email(
                'partner',
                [obj.email],
                conf.subject or str(self.get_event_name(request, obj)),
                params={
                    'conf': conf,
                    'obj': obj,
                    'request': request,
                    'site_url': site_url()
                },
                raise_error=False
            )

    @swagger_auto_schema(
        request_body=serializer_class,
        responses={
            200: openapi_responses.success_response,
            400: openapi_responses.validation_error_response,
        }
    )
    def post(self, request, lang, **kwargs):
        return super(UserRequestView, self).post(request, lang, **kwargs)
