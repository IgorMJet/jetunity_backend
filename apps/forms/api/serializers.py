from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from forms import models
from handbooks.models import Airport
from jetsharing.models import JetSharingFlight
from snippets.api.serializers.fields import SlugRelatedToPkField
from users.models import User


class BaseRequestSerializer(serializers.ModelSerializer):

    role_flag = None

    class Meta:
        fields = ('created', 'id', 'language')
        model = None
        read_only_fields = ('id', 'created')

    def validate(self, data):
        email = data.get('email')
        referral_code = data.get('referral_code')

        if referral_code:
            referral_code = referral_code.lower().strip()
            try:
                user = User.objects.filter(is_active=True)\
                    .get(referral_code=referral_code)
                data['referral_user'] = user
            except User.DoesNotExist:
                pass

            del data['referral_code']

        with transaction.atomic():
            if email:
                email = data['email'] = email.strip().lower()
                try:
                    user = User.objects.get(username=email)
                except User.DoesNotExist:
                    user = User(
                        username=email,
                        email=email,
                        first_name=data.get(
                            'name', data.get('first_name', None)
                        ),
                        last_name=data.get('last_name', ''),
                        phone_number=data.get('phone_number')
                    )
                    user.set_unusable_password()
                    user.save()

                if self.role_flag and not getattr(user, self.role_flag):
                    setattr(user, self.role_flag, True)
                    user.save()
                data['user'] = user
                if user == data.get('referral_user'):
                    del data['referral_user']
        return data


class BookingFlightSerializer(BaseRequestSerializer):
    """Рейс бронирования"""

    origin_text = serializers.CharField(
        label='Аэропорт вылета (текст)', max_length=255, required=True
    )
    destination_text = serializers.CharField(
        label='Аэропорт прибытия (текст)', max_length=255, required=True
    )
    flight_date = serializers.DateField(label='Дата вылета', required=True)

    class Meta:
        fields = (
            'created', 'destination_text', 'flight_date', 'origin_text', 'id'
        )
        model = models.BookingFlight
        read_only_fields = ('created', 'id')


class BookingSerializer(BaseRequestSerializer):
    """Бронирования"""

    email = serializers.EmailField(
        label=_('Email'), max_length=255, required=True
    )
    first_name = serializers.CharField(
        label=_('Имя'), max_length=255, required=True
    )
    last_name = serializers.CharField(
        label=_('Фамилия'), max_length=255, required=False, allow_null=True,
        allow_blank=True
    )
    phone_number = serializers.CharField(
        label=_('Номер телефона'), max_length=18, required=True
    )
    referral_code = serializers.CharField(
        label=_('Код реферрала'), max_length=8, required=False,
        allow_null=True, allow_blank=True
    )
    role_flag = 'is_customer'

    class Meta:
        fields = (
            'created', 'email', 'first_name', 'last_name', 'id', 'language',
            'phone_number', 'referral_code', 'utm_source', 'utm_medium',
            'utm_campaign', 'utm_content', 'utm_term'
        )
        model = models.Booking
        read_only_fields = ('created', 'id')


class BrokerRequestSerializer(BaseRequestSerializer):
    """Заявки брокеров"""

    email = serializers.EmailField(
        label=_('Email'), max_length=255, required=True
    )
    name = serializers.CharField(
        label=_('Полное имя'), max_length=255, required=True
    )
    phone_number = serializers.CharField(
        label=_('Номер телефона'), max_length=18, required=True
    )
    referral_code = serializers.CharField(
        label=_('Код реферрала'), max_length=8, required=False,
        allow_null=True, allow_blank=True
    )
    url = serializers.CharField(
        label=_('Страница'), max_length=255, required=False, allow_null=True
    )
    role_flag = 'is_broker'

    class Meta:
        fields = (
            'created', 'email', 'id', 'language', 'name', 'phone_number',
            'referral_code', 'url', 'utm_source', 'utm_medium', 'utm_campaign',
            'utm_content', 'utm_term'
        )
        model = models.BrokerRequest
        read_only_fields = ('created', 'id')


class JetSharingBookingSerializer(BaseRequestSerializer):
    """Бронирования JetSharing"""

    email = serializers.EmailField(
        label=_('Email'), max_length=255, required=True
    )
    first_name = serializers.CharField(
        label=_('Имя'), max_length=255, required=True
    )
    last_name = serializers.CharField(
        label=_('Фамилия'), max_length=255, required=False, allow_null=True,
        allow_blank=True
    )
    phone_number = serializers.CharField(
        label=_('Номер телефона'), max_length=18, required=True
    )
    seats = serializers.IntegerField(
        label=_('Количество мест'), min_value=1, required=True
    )
    want_return_flight = serializers.BooleanField(
        label=_('Обсудить обратный рейс'), default=False
    )
    referral_code = serializers.CharField(
        label=_('Код реферрала'), max_length=8, required=False,
        allow_null=True, allow_blank=True
    )
    role_flag = 'is_customer'

    class Meta:
        fields = (
            'created', 'email', 'first_name', 'last_name', 'id', 'language',
            'phone_number', 'referral_code', 'seats', 'want_return_flight',
            'utm_source', 'utm_medium', 'utm_campaign', 'utm_content',
            'utm_term'
        )
        model = models.JetSharingBooking
        read_only_fields = ('created', 'id')


class JetSharingBookingFlightSerializer(BaseRequestSerializer):
    """Рейс бронирования JetSharing"""

    airport_origin_id = SlugRelatedToPkField(
        queryset=Airport.objects.published(), write_only=True,
        allow_null=True, slug_field='id'
    )
    airport_destination_id = SlugRelatedToPkField(
        queryset=Airport.objects.published(), write_only=True,
        allow_null=True, slug_field='id'
    )
    destination_datetime = serializers.DateTimeField()
    origin_datetime = serializers.DateTimeField()
    price = serializers.DecimalField(max_digits=11, decimal_places=2)

    class Meta:
        fields = (
            'airport_origin_id', 'airport_destination_id', 'created',
            'destination_datetime', 'id', 'origin_datetime', 'price'
        )
        model = models.JetSharingBookingFlight
        read_only_fields = ('created', 'id')


class PartnershipRequestSerializer(BaseRequestSerializer):
    """Заявки на сотрудничество"""

    email = serializers.EmailField(
        label=_('Email'), max_length=255, required=True
    )
    name = serializers.CharField(
        label=_('Полное имя'), max_length=255, required=True
    )
    phone_number = serializers.CharField(
        label=_('Номер телефона'), max_length=18, required=True
    )
    referral_code = serializers.CharField(
        label=_('Код реферрала'), max_length=8, required=False,
        allow_null=True, allow_blank=True
    )
    url = serializers.CharField(
        label=_('Страница'), max_length=255, required=False, allow_null=True
    )
    role_flag = 'is_partner'

    class Meta:
        fields = (
            'created', 'email', 'id', 'language', 'name', 'phone_number',
            'referral_code', 'url', 'utm_source', 'utm_medium', 'utm_campaign',
            'utm_content', 'utm_term'
        )
        model = models.PartnershipRequest
        read_only_fields = ('created', 'id')


class UserRequestSerializer(BaseRequestSerializer):
    """Заявки на сотрудничество"""

    email = serializers.EmailField(
        label=_('Email'), max_length=255, required=True
    )
    name = serializers.CharField(
        label=_('Полное имя'), max_length=255, required=True
    )
    phone_number = serializers.CharField(
        label=_('Номер телефона'), max_length=18, required=True
    )
    referral_code = serializers.CharField(
        label=_('Код реферрала'), max_length=8, required=False,
        allow_null=True, allow_blank=True
    )
    url = serializers.CharField(
        label=_('Страница'), max_length=255, required=False, allow_null=True
    )
    role_flag = 'is_customer'

    class Meta:
        fields = (
            'created', 'email', 'id', 'language', 'name', 'phone_number',
            'referral_code', 'url', 'utm_source', 'utm_medium', 'utm_campaign',
            'utm_content', 'utm_term'
        )
        model = models.UserRequest
        read_only_fields = ('created', 'id')
