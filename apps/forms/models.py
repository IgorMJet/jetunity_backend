from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from forms.choices import FormRequestReadStatusChoices
from forms.utils import generate_booking_code, generate_jetsharing_booking_code
from snippets.models import LastModMixin, BasicModel, BaseModel


class BaseFormRequest(LastModMixin, BasicModel):
    """Base model for all request forms"""

    language = models.CharField(
        'Язык', max_length=6, default=settings.DEFAULT_LANGUAGE,
        choices=settings.LANGUAGES
    )
    read_status = models.SmallIntegerField(
        'Статус прочтение', choices=FormRequestReadStatusChoices.choices,
        default=FormRequestReadStatusChoices.UNREAD
    )
    utm_source = models.CharField(
        'UTM Source', max_length=255, blank=True, null=True
    )
    utm_medium = models.CharField(
        'UTM Medium', max_length=255, blank=True, null=True
    )
    utm_campaign = models.CharField(
        'UTM Campaign', max_length=255, blank=True, null=True
    )
    utm_content = models.CharField(
        'UTM Content', max_length=255, blank=True, null=True
    )
    utm_term = models.CharField(
        'UTM Term', max_length=255, blank=True, null=True
    )

    email_fields = ('language',)

    class Meta:
        abstract = True


class Booking(BaseFormRequest):
    """Бронирования"""

    booking_code = models.CharField(
        'Номер брони', max_length=8, default=generate_booking_code, unique=True
    )
    first_name = models.CharField('Имя', max_length=255, blank=True, null=True)
    last_name = models.CharField(
        'Фамилия', max_length=255, blank=True, null=True
    )
    phone_number = models.CharField(
        'Телефон', max_length=18, blank=True, null=True
    )
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    user = models.ForeignKey(
        'users.User', verbose_name='Пользователь', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='bookings_as_user'
    )
    referral_user = models.ForeignKey(
        'users.User', verbose_name='Реферрал (пригласивший)', blank=True,
        null=True, on_delete=models.SET_NULL,
        related_name='bookings_as_referral'
    )

    email_fields = BaseFormRequest.email_fields + (
        'first_name', 'last_name', 'phone_number', 'email', 'user',
        'referral_user', ('flights_text', _('Рейсы'))
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Бронирование'
        verbose_name_plural = 'Бронирования'

    def __str__(self):
        return str(self.id)

    @property
    def flights_text(self):
        return '\n'.join([
            f'{f.origin_text or "-"} - {f.destination_text or "-"} '
            f'{f.flight_date or "-"}'
            for f in self.flights.all()
        ])


class BookingFlight(BaseModel):
    """Рейсы бронирования"""

    booking = models.ForeignKey(
        'forms.Booking', related_name='flights', verbose_name='Бронирование',
        on_delete=models.CASCADE
    )
    origin_text = models.CharField(
        'Аэропорт вылета (текст)', max_length=255, blank=True, null=True
    )
    destination_text = models.CharField(
        'Аэропорт прибытия (текст)', max_length=255, blank=True, null=True
    )
    flight_date = models.DateField('Дата вылета', blank=True, null=True)

    class Meta:
        ordering = ('created',)
        verbose_name = 'Рейс бронирования'
        verbose_name_plural = 'Рейсы бронирования'

    def __str__(self):
        return str(self.id)


class JetSharingBooking(BaseFormRequest):
    """Бронирования JetSharing"""

    booking_code = models.CharField(
        'Номер брони', max_length=8, default=generate_jetsharing_booking_code,
        unique=True
    )
    first_name = models.CharField('Имя', max_length=255, blank=True, null=True)
    last_name = models.CharField(
        'Фамилия', max_length=255, blank=True, null=True
    )
    phone_number = models.CharField(
        'Телефон', max_length=18, blank=True, null=True
    )
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    seats = models.PositiveSmallIntegerField('Количество мест', default=1)
    want_return_flight = models.BooleanField(
        'Клиент хочет обсудить обратный рейс', default=False
    )
    user = models.ForeignKey(
        'users.User', verbose_name='Пользователь', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='jetsharing_bookings_as_user'
    )
    referral_user = models.ForeignKey(
        'users.User', verbose_name='Реферрал (пригласивший)', blank=True,
        null=True, on_delete=models.SET_NULL,
        related_name='jetsharing_bookings_as_referral'
    )

    email_fields = BaseFormRequest.email_fields + (
        'first_name', 'last_name', 'phone_number', 'email', 'user',
        'referral_user', ('flights_text', _('Рейсы'))
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Бронирование JetSharing'
        verbose_name_plural = 'Бронирования JetSharing'

    def __str__(self):
        return str(self.id)

    @property
    def flights_text(self):
        return '\n'.join([
            f'{f.airport_origin or "-"} - '
            f'{f.airport_destination or "-"} '
            f'({f.origin_datetime or "-"} - '
            f'{f.destination_datetime or "-"})'
            for f in self.flights.select_related(
                'airport_origin', 'airport_destination',
                'airport_origin__city', 'airport_destination__city'
            )
        ])


class JetSharingBookingFlight(BaseModel):
    """Рейсы бронирования"""

    booking = models.ForeignKey(
        'forms.JetSharingBooking', related_name='flights',
        verbose_name='Бронирование', on_delete=models.CASCADE
    )
    airport_origin = models.ForeignKey(
        'handbooks.Airport', related_name='bookings_as_origin',
        verbose_name='Аэропорт вылета', on_delete=models.CASCADE
    )
    airport_destination = models.ForeignKey(
        'handbooks.Airport', related_name='bookings_as_destinations',
        verbose_name='Аэропорт прибытия', on_delete=models.CASCADE
    )
    origin_datetime = models.DateTimeField('Время вылета')
    destination_datetime = models.DateTimeField('Время прибытия')
    price = models.DecimalField('Цена', max_digits=11, decimal_places=2)

    class Meta:
        ordering = ('created',)
        verbose_name = 'Рейс бронирования'
        verbose_name_plural = 'Рейсы бронирования'

    def __str__(self):
        return str(self.id)

    @property
    def origin_text(self):
        return str(self.airport_origin)

    @property
    def destination_text(self):
        return str(self.airport_destination)

    @property
    def flight_date(self):
        return self.origin_datetime


class BrokerRequest(BaseFormRequest):
    """Запросы брокеров"""

    name = models.CharField(
        'Полное имя', max_length=255, blank=True, null=True
    )
    phone_number = models.CharField(
        'Телефон', max_length=18, blank=True, null=True
    )
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    url = models.CharField('Страница', max_length=255, blank=True, null=True)
    user = models.ForeignKey(
        'users.User', verbose_name='Пользователь', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='broker_requests_as_user'
    )
    referral_user = models.ForeignKey(
        'users.User', verbose_name='Реферрал (пригласивший)', blank=True,
        null=True, on_delete=models.SET_NULL,
        related_name='broker_requests_as_referral'
    )

    email_fields = BaseFormRequest.email_fields + (
        'name', 'phone_number', 'email', 'url', 'user', 'referral_user'
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заявка брокера'
        verbose_name_plural = 'Заявки брокеров'

    def __str__(self):
        return f'{self.name or ""} ({self.phone_number or "-"})'


class PartnershipRequest(BaseFormRequest):
    """Запросы на сотрудничество"""

    name = models.CharField(
        'Полное имя', max_length=255, blank=True, null=True
    )
    phone_number = models.CharField(
        'Телефон', max_length=18, blank=True, null=True
    )
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    url = models.CharField('Страница', max_length=255, blank=True, null=True)
    user = models.ForeignKey(
        'users.User', verbose_name='Пользователь', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='partnership_requests_as_user'
    )
    referral_user = models.ForeignKey(
        'users.User', verbose_name='Реферрал (пригласивший)', blank=True,
        null=True, on_delete=models.SET_NULL,
        related_name='partnership_requests_as_referral'
    )

    email_fields = BaseFormRequest.email_fields + (
        'name', 'phone_number', 'email', 'url', 'user', 'referral_user'
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заявка на сотрудничество'
        verbose_name_plural = 'Заявки на сотрудничество'

    def __str__(self):
        return f'{self.name or ""} ({self.phone_number or "-"})'


class UserRequest(BaseFormRequest):
    """Запросы на регистрацию пользователей"""

    name = models.CharField(
        'Полное имя', max_length=255, blank=True, null=True
    )
    phone_number = models.CharField(
        'Телефон', max_length=18, blank=True, null=True
    )
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    url = models.CharField('Страница', max_length=255, blank=True, null=True)
    user = models.ForeignKey(
        'users.User', verbose_name='Пользователь', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='user_requests_as_user'
    )
    referral_user = models.ForeignKey(
        'users.User', verbose_name='Реферрал (пригласивший)', blank=True,
        null=True, on_delete=models.SET_NULL,
        related_name='user_requests_as_referral'
    )

    email_fields = BaseFormRequest.email_fields + (
        'name', 'phone_number', 'email', 'url', 'user', 'referral_user'
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заявка на регистрацию пользователя'
        verbose_name_plural = 'Заявки на регистрацию пользователей'

    def __str__(self):
        return f'{self.name or ""} ({self.phone_number or "-"})'
