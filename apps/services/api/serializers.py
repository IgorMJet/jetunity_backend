from rest_framework import serializers

from services import models
from snippets.api.serializers import fields
from snippets.api.serializers.mixins import SEOSerializerMixin


class ServiceFeatureSerializer(serializers.ModelSerializer):
    """Возможности на странице услуги"""

    icon = fields.ImageField()

    class Meta:
        fields = ('description', 'icon', 'id')
        model = models.ServiceFeature


class ServicesPageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Страница услуг"""

    hero_image = fields.ImageField()
    meta = serializers.SerializerMethodField()

    class Meta:
        model = models.ServicesPage
        fields = ('hero_image', 'hero_title', 'meta', 'title')


class ServiceListSerializer(serializers.ModelSerializer):
    """Услуга в списке"""

    list_description = fields.PretextField()
    list_image = fields.ImageField()
    image = fields.ImageField()

    class Meta:
        model = models.Service
        fields = (
            'id', 'image', 'list_description', 'list_image', 'slug', 'title'
        )


class ServiceSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Услуга"""

    features = fields.PublishedRelationField(ServiceFeatureSerializer, many=True)
    image = fields.ImageField()
    meta = serializers.SerializerMethodField()
    services_image_1 = fields.ImageField()
    services_image_2 = fields.ImageField()
    services_image_3 = fields.ImageField()

    class Meta:
        model = models.Service
        fields = (
            'content_1', 'content_2', 'content_3', 'content_button_caption', 'content_button_link',
            'content_title', 'features', 'features_title', 'id', 'image', 'meta',
            'services_description_content', 'services_description_title',
            'services_description_title_link', 'services_image_1', 'services_image_2',
            'services_image_3', 'services_title', 'show_features', 'show_services', 'slug', 'title'
        )
