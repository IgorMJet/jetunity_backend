from django.urls import path

from services.api import resources


app_name = 'services'

urlpatterns = (
    path(
        'page/',
        resources.ServicesPageView.as_view(),
        name='services_page'
    ),
    path(
        'services/',
        resources.ServicesListView.as_view(),
        name='services'
    ),
    path(
        'services/<slug:slug>/',
        resources.ServiceView.as_view(),
        name='service'
    )
)
