from services import models
from services.api import serializers
from snippets.api.views import PublicViewMixin, PageRetrieveAPIView,\
    BaseRetrieveAPIView, BaseListAPIView


class ServiceView(PublicViewMixin, BaseRetrieveAPIView):
    """Услуга"""

    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
    queryset = models.Service.objects.published()
    serializer_class = serializers.ServiceSerializer


class ServicesListView(PublicViewMixin, BaseListAPIView):
    """Список услуг"""

    queryset = models.Service.objects.published()
    serializer_class = serializers.ServiceListSerializer


class ServicesPageView(PublicViewMixin, PageRetrieveAPIView):
    """Страница услуг"""

    queryset = models.ServicesPage.objects.all()
    serializer_class = serializers.ServicesPageSerializer
