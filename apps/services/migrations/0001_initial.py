# Generated by Django 3.0.7 on 2020-07-10 15:55

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('ordering', models.IntegerField(db_index=True, default=0, verbose_name='Порядок')),
                ('status', models.SmallIntegerField(choices=[(0, 'Черновик'), (1, 'Публичный'), (2, 'Скрытый')], default=1, verbose_name='Статус')),
                ('seo_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_description', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('seo_description_ru', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('seo_description_en', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('title_ru', models.CharField(max_length=255, null=True, verbose_name='Заголовок')),
                ('title_en', models.CharField(max_length=255, null=True, verbose_name='Заголовок')),
                ('slug', models.SlugField(help_text='Латинские буквы и цифры, подчеркивание и дефис', max_length=150, unique=True, verbose_name='Алиас')),
                ('list_image', models.ImageField(blank=True, max_length=255, null=True, upload_to='services', verbose_name='Изображение в списке')),
                ('list_description', models.TextField(blank=True, null=True, verbose_name='Описание в списке')),
                ('list_description_ru', models.TextField(blank=True, null=True, verbose_name='Описание в списке')),
                ('list_description_en', models.TextField(blank=True, null=True, verbose_name='Описание в списке')),
                ('image', models.ImageField(blank=True, max_length=255, null=True, upload_to='services', verbose_name='Изображение на странице услуги')),
                ('content_title', models.TextField(blank=True, max_length=1024, null=True, verbose_name='Заголовок контента')),
                ('content_title_ru', models.TextField(blank=True, max_length=1024, null=True, verbose_name='Заголовок контента')),
                ('content_title_en', models.TextField(blank=True, max_length=1024, null=True, verbose_name='Заголовок контента')),
                ('content_1', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 1')),
                ('content_1_ru', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 1')),
                ('content_1_en', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 1')),
                ('content_2', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 2')),
                ('content_2_ru', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 2')),
                ('content_2_en', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 2')),
                ('content_3', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 3')),
                ('content_3_ru', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 3')),
                ('content_3_en', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Контент 3')),
                ('content_button_caption', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кпопки в контенте')),
                ('content_button_caption_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кпопки в контенте')),
                ('content_button_caption_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кпопки в контенте')),
                ('content_button_link', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кпопки в контенте')),
                ('content_button_link_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кпопки в контенте')),
                ('content_button_link_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кпопки в контенте')),
                ('show_services', models.BooleanField(default=True, verbose_name='Показывать услуги')),
                ('services_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок')),
                ('services_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок')),
                ('services_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок')),
                ('services_image_1', models.ImageField(blank=True, max_length=255, null=True, upload_to='services/images', verbose_name='Услуги: изображение 1')),
                ('services_image_2', models.ImageField(blank=True, max_length=255, null=True, upload_to='services/images', verbose_name='Услуги: изображение 2')),
                ('services_image_3', models.ImageField(blank=True, max_length=255, null=True, upload_to='services/images', verbose_name='Услуги: изображение 3')),
                ('services_description_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок блока под изображениями')),
                ('services_description_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок блока под изображениями')),
                ('services_description_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Услуги: заголовок блока под изображениями')),
                ('services_description_content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Услуги: контент блока под изображениями')),
                ('services_description_content_ru', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Услуги: контент блока под изображениями')),
                ('services_description_content_en', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Услуги: контент блока под изображениями')),
                ('show_features', models.BooleanField(default=True, verbose_name='Показывать возможности')),
                ('features_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Возможности: заголовок')),
                ('features_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Возможности: заголовок')),
                ('features_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Возможности: заголовок')),
            ],
            options={
                'verbose_name': 'Услуга',
                'verbose_name_plural': 'Услуги',
            },
        ),
        migrations.CreateModel(
            name='ServicesPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('seo_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_description', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('hero_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_image', models.ImageField(blank=True, max_length=255, null=True, upload_to='services/page', verbose_name='Первый экран: изображение')),
            ],
            options={
                'verbose_name': 'Страница услуг',
                'verbose_name_plural': 'Страница услуг',
            },
        ),
        migrations.CreateModel(
            name='ServiceFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('ordering', models.IntegerField(db_index=True, default=0, verbose_name='Порядок')),
                ('status', models.SmallIntegerField(choices=[(0, 'Черновик'), (1, 'Публичный'), (2, 'Скрытый')], default=1, verbose_name='Статус')),
                ('icon', models.FileField(blank=True, max_length=255, null=True, upload_to='service/features', verbose_name='Иконка')),
                ('title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='features', to='services.Service', verbose_name='Страница')),
            ],
            options={
                'verbose_name': 'Возможность на странице услуги',
                'verbose_name_plural': 'Возможности на странице услуги',
                'ordering': ('ordering',),
            },
        ),
    ]
