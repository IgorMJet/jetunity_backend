from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField

from snippets.models import BaseModel
from snippets.models.image import ImageMixin
from snippets.models.pages import BasePage
from snippets.models.seo import SEOModelMixin


class ServicesPage(BasePage):
    """Страница услуг"""

    hero_title = models.CharField('Первый экран: заголовок', max_length=255, blank=True, null=True)
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='services/page', blank=True,
        null=True
    )

    translation_fields = BasePage.translation_fields + ('hero_title',)

    class Meta:
        verbose_name = 'Страница услуг'
        verbose_name_plural = 'Страница услуг'

    def __str__(self):
        return 'Страница услуг'


class Service(ImageMixin, SEOModelMixin, BaseModel):
    """Услуги"""

    image_field = 'list_image'

    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField(
        'Алиас', max_length=150, db_index=True, unique=True,
        help_text='Латинские буквы и цифры, подчеркивание и дефис'
    )
    list_image = models.ImageField(
        'Изображение в списке', max_length=255, upload_to='services', blank=True, null=True
    )
    list_description = models.TextField('Описание в списке', blank=True, null=True)
    image = models.ImageField(
        'Изображение на странице услуги', max_length=255, upload_to='services', blank=True,
        null=True
    )
    content_title = models.TextField('Заголовок контента', max_length=1024, blank=True, null=True)
    content_1 = RichTextUploadingField('Контент 1', blank=True, null=True)
    content_2 = RichTextUploadingField('Контент 2', blank=True, null=True)
    content_3 = RichTextUploadingField('Контент 3', blank=True, null=True)
    content_button_caption = models.CharField(
        'Текст кпопки в контенте', max_length=255, blank=True, null=True
    )
    content_button_link = models.CharField(
        'Ссылка кпопки в контенте', max_length=255, blank=True, null=True
    )
    show_services = models.BooleanField('Показывать услуги', default=True)
    services_title = models.CharField('Услуги: заголовок', max_length=255, blank=True, null=True)
    services_image_1 = models.ImageField(
        'Услуги: изображение 1', max_length=255, upload_to='services/images', blank=True, null=True
    )
    services_image_2 = models.ImageField(
        'Услуги: изображение 2', max_length=255, upload_to='services/images', blank=True, null=True
    )
    services_image_3 = models.ImageField(
        'Услуги: изображение 3', max_length=255, upload_to='services/images', blank=True, null=True
    )
    services_description_title = models.CharField(
        'Услуги: заголовок блока под изображениями', max_length=255, blank=True, null=True
    )
    services_description_title_link = models.CharField(
        'Услуги: ссылка заголовка блока под изображениями', max_length=255, blank=True, null=True
    )
    services_description_content = RichTextUploadingField(
        'Услуги: контент блока под изображениями', blank=True, null=True
    )
    show_features = models.BooleanField('Показывать возможности', default=True)
    features_title = models.CharField(
        'Возможности: заголовок', max_length=255, blank=True, null=True
    )

    translation_fields = SEOModelMixin.translation_fields + (
        'content_1', 'content_2', 'content_3', 'content_button_caption', 'content_button_link',
        'content_title', 'features_title', 'list_description', 'services_description_content',
        'services_description_title', 'services_description_title_link', 'services_title', 'title'
    )

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return self.title


class ServiceFeature(BaseModel):
    """Возможности на странице услуги"""

    page = models.ForeignKey(
        'services.Service', verbose_name='Страница', related_name='features',
        on_delete=models.CASCADE
    )
    icon = models.FileField(
        'Иконка', max_length=255, upload_to='service/features', blank=True, null=True
    )
    description = RichTextUploadingField('Описание', blank=True, null=True)

    translation_fields = ('description',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Возможность на странице услуги'
        verbose_name_plural = 'Возможности на странице услуги'

    def __str__(self):
        return str(self.pk)
