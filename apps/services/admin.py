from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from solo.admin import SingletonModelAdmin

from services import models
from snippets.admin import BaseModelAdmin
from snippets.utils.modeltranslation import get_model_translation_fields


@admin.register(models.ServicesPage)
class ServicesPageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Страница услуг"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': ('hero_title', 'hero_image')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('seo', 'SEO')
    )


class ServiceFeatureInline(TranslationStackedInline):
    """Возможности на странице услуги"""

    extra = 0
    fields = models.ServiceFeature().collect_fields()
    model = models.ServiceFeature
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-features'


@admin.register(models.Service)
class ServiceAdmin(BaseModelAdmin, TranslationAdmin):
    """Услуги"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'slug', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-images'),
            'fields': ('list_image', 'image')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-content'),
            'fields': (
                'list_description', 'content_title', 'content_1', 'content_2', 'content_3',
                'content_button_caption', 'content_button_link'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-services'),
            'fields': (
                'show_services', 'services_title', 'services_image_1', 'services_image_2',
                'services_image_3', 'services_description_title',
                'services_description_title_link', 'services_description_content'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-features'),
            'fields': ('show_features', 'features_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (ServiceFeatureInline,)
    list_display = ('image_thumb', 'title', 'slug', 'status', 'ordering', 'created')
    list_display_links = ('image_thumb', 'title')
    search_fields = ['=id', 'slug'] + get_model_translation_fields(models.Service)
    suit_form_tabs = (
        ('general', 'Основное'),
        ('images', 'Изображения'),
        ('content', 'Описание'),
        ('services', 'Услуги'),
        ('features', 'Возможности'),
        ('seo', 'SEO')
    )

    class Media:
        js = ('admin/js/translit.js',)
