from modeltranslation.decorators import register

from brokers import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.BrokersPage)
class BrokersPageTranslationOptions(BaseTranslationOptions):
    fields = models.BrokersPage.translation_fields
    required_languages = {'default': ()}


@register(models.BrokersPageFeature)
class BrokersPageFeatureTranslationOptions(BaseTranslationOptions):
    fields = models.BrokersPageFeature.translation_fields
    required_languages = {'default': ()}


@register(models.BrokersPageSlide)
class BrokersPageSlideTranslationOptions(BaseTranslationOptions):
    fields = models.BrokersPageSlide.translation_fields
    required_languages = {'default': ()}
