from rest_framework import serializers

from brokers import models
from core.api.serializers import GallerySerializer
from snippets.api.serializers import fields
from snippets.api.serializers.mixins import SEOSerializerMixin


class BrokersPageFeatureSerializer(serializers.ModelSerializer):
    """Преимущества на странице "Брокеры\""""

    icon = fields.ImageField()

    class Meta:
        model = models.BrokersPageFeature
        fields = ('description', 'icon', 'id')


class BrokersPageSlideSerializer(serializers.ModelSerializer):
    """Слайды на странице "Брокеры\""""

    image = fields.ImageField()

    class Meta:
        model = models.BrokersPageSlide
        fields = (
            'button_caption', 'button_url', 'description', 'id', 'image',
            'title'
        )


class BrokersPageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Страница "Брокеры\""""

    about_gallery = fields.PublishedRelationField(GallerySerializer, many=False)
    features = fields.PublishedRelationField(BrokersPageFeatureSerializer, many=True)
    form_bg = fields.ImageField()
    form_bg_2x = fields.ImageField()
    form_sent_to_email = fields.PretextField()
    hero_bg = fields.ImageField()
    meta = serializers.SerializerMethodField()
    slides = fields.PublishedRelationField(BrokersPageSlideSerializer, many=True)

    class Meta:
        model = models.BrokersPage
        fields = (
            'about_content', 'about_gallery', 'about_title', 'features', 'features_title',
            'form_bg', 'form_bg_2x', 'form_copy_button', 'form_sent_to_email', 'form_share_button',
            'form_subtitle', 'form_success_message_content', 'form_success_message_title',
            'form_title', 'hero_bg', 'hero_button_caption', 'hero_button_link', 'hero_subtitle',
            'hero_title', 'meta', 'show_about', 'show_about_gallery', 'show_features', 'show_form',
            'slides', 'title'
        )
