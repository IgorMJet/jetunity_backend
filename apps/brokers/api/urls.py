from django.urls import path

from brokers.api import resources


app_name = 'brokers'

urlpatterns = (
    path(
        'page/',
        resources.BrokersPageView.as_view(),
        name='brokers_page'
    ),
)
