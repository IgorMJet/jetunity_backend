from brokers import models
from brokers.api import serializers
from snippets.api.views import PublicViewMixin, PageRetrieveAPIView


class BrokersPageView(PublicViewMixin, PageRetrieveAPIView):
    """Страница "Брокеры\""""

    queryset = models.BrokersPage.objects.all()
    serializer_class = serializers.BrokersPageSerializer
