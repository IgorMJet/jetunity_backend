from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'brokers'
    verbose_name = 'Брокеры'
