from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField

from core.models import ReferralCodeFormPageMixin
from snippets.models import BaseModel
from snippets.models.pages import BasePage


class BrokersPage(ReferralCodeFormPageMixin, BasePage):
    """Страница "Брокеры\""""

    hero_title = models.CharField('Первый экран: заголовок', max_length=255, blank=True, null=True)
    hero_subtitle = models.CharField(
        'Первый экран: подзаголовок', max_length=255, blank=True, null=True
    )
    hero_button_caption = models.CharField(
        'Первый экран: текст кнопки', max_length=255, blank=True, null=True
    )
    hero_button_link = models.CharField(
        'Первый экран: ссылка кнопки', max_length=255, blank=True, null=True
    )
    hero_bg = models.ImageField(
        'Первый экран: фон', max_length=255, upload_to='brokers/bgs', blank=True, null=True
    )
    show_about = models.BooleanField('Показывать блок О сообществе', default=True)
    about_title = models.CharField(
        'О сообществе: заголовок', max_length=255, blank=True, null=True
    )
    about_content = RichTextUploadingField('О сообществе: контент', blank=True, null=True)
    show_about_gallery = models.BooleanField('О сообществе: показывать галерею', default=True)
    about_gallery = models.ForeignKey(
        'core.Gallery', verbose_name='О сообществе: галерея', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    show_features = models.BooleanField('Показывать блок преимуществ', default=True)
    features_title = models.CharField(
        'Преимущества: заголовок', max_length=255, blank=True, null=True
    )
    show_form = models.BooleanField('Показывать блок формы', default=True)
    form_title = models.CharField('Форма: заголовок', max_length=255, blank=True, null=True)
    form_subtitle = models.CharField('Форма: подзаголовок', max_length=255, blank=True, null=True)
    form_bg = models.ImageField(
        'Форма: фон', max_length=255, upload_to='brokers/bgs', blank=True, null=True
    )
    form_bg_2x = models.ImageField(
        'Форма: фон (двойное качество)', max_length=255, upload_to='brokers/bgs', blank=True,
        null=True
    )

    translation_fields = BasePage.translation_fields +\
        ReferralCodeFormPageMixin.translation_fields + (
            'about_content', 'about_title', 'features_title', 'form_subtitle', 'form_title',
            'hero_button_caption', 'hero_button_link', 'hero_subtitle', 'hero_title'
        )

    class Meta:
        verbose_name = 'Страница "Брокеры"'
        verbose_name_plural = 'Страница "Брокеры"'

    def __str__(self):
        return 'Страница "Брокеры"'


class BrokersPageFeature(BaseModel):
    """Преимущества на странице "Брокеры\""""

    page = models.ForeignKey(
        'brokers.BrokersPage', verbose_name='Страница', related_name='features',
        on_delete=models.CASCADE
    )
    icon = models.FileField(
        'Иконка', max_length=255, upload_to='brokers/features', blank=True, null=True
    )
    description = RichTextUploadingField('Описание', blank=True, null=True)

    translation_fields = ('description',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Преимущество'
        verbose_name_plural = 'Преимущества'

    def __str__(self):
        return str(self.pk)


class BrokersPageSlide(BaseModel):
    """Слайды на странице "Брокеры\""""

    page = models.ForeignKey(
        'brokers.BrokersPage', verbose_name='Страница', related_name='slides',
        on_delete=models.CASCADE
    )
    image = models.ImageField(
        'Изображение', max_length=255, upload_to='brokers/slides', blank=True, null=True
    )
    title = models.CharField('Заголовок', max_length=255, blank=True, null=True)
    description = RichTextUploadingField('Описание', blank=True, null=True)
    button_caption = models.CharField('Текст "Подробнее"', max_length=255, blank=True, null=True)
    button_url = models.CharField('Ссылка "Подробнее"', max_length=255, blank=True, null=True)

    translation_fields = ('button_caption', 'button_url', 'description', 'title')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды на странице "Брокеры\"'

    def __str__(self):
        return self.title or str(self.pk)
