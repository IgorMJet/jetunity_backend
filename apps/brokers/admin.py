from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from solo.admin import SingletonModelAdmin

from brokers import models


class BrokersPageFeatureInline(TranslationStackedInline):
    """Преимущества на страница "Брокеры\""""

    extra = 0
    fields = models.BrokersPageFeature().collect_fields()
    model = models.BrokersPageFeature
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-features'


class BrokersPageSlideInline(TranslationStackedInline):
    """Слайды на страница "Брокеры\""""

    extra = 0
    fields = models.BrokersPageSlide().collect_fields()
    model = models.BrokersPageSlide
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-slides'


@admin.register(models.BrokersPage)
class BrokersPageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Страница "Брокеры\""""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': (
                'hero_title', 'hero_subtitle', 'hero_button_caption', 'hero_button_link', 'hero_bg'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-about'),
            'fields': (
                'show_about', 'about_title', 'about_content', 'show_about_gallery', 'about_gallery'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-features'),
            'fields': ('show_features', 'features_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-form'),
            'fields': (
                'show_form', 'form_title', 'form_subtitle', 'form_bg', 'form_bg_2x',
                'form_success_message_title', 'form_success_message_content', 'form_share_button',
                'form_copy_button', 'form_sent_to_email'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (BrokersPageFeatureInline, BrokersPageSlideInline)
    raw_id_fields = ('about_gallery',)
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('about', 'О сообществе'),
        ('features', 'Преимущества'),
        ('slides', 'Слайды'),
        ('form', 'Форма'),
        ('seo', 'SEO')
    )
