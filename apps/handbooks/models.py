from django.db import models

from snippets.models import BaseDictionaryModel


class Airport(BaseDictionaryModel):
    """Аэропорт"""

    code = models.CharField('Код', max_length=255, unique=True)
    title = models.CharField(
        'Заголовок', max_length=400, blank=True, null=True
    )
    city = models.ForeignKey(
        'handbooks.City', verbose_name='Город', related_name='airports',
        on_delete=models.SET_NULL, blank=True, null=True
    )

    class Meta:
        ordering = ('ordering', 'title')
        verbose_name = 'Аэропорт'
        verbose_name_plural = 'Аэропорты'

    def __str__(self):
        return f'{self.code} {self.title} ({self.city})'


class City(BaseDictionaryModel):
    """Города"""

    class Meta:
        ordering = ('ordering', 'title')
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class PlaneModel(BaseDictionaryModel):
    """Модели самолетов"""

    code = models.CharField('Код', max_length=255, unique=True)

    class Meta:
        ordering = ('ordering', 'title')
        verbose_name = 'Модель самолета'
        verbose_name_plural = 'Модели самолетов'

