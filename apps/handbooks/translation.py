from modeltranslation.decorators import register

from handbooks import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.Airport)
class AirportTranslationOptions(BaseTranslationOptions):
    fields = models.Airport.translation_fields
    required_languages = {'default': ()}


@register(models.City)
class CityTranslationOptions(BaseTranslationOptions):
    fields = models.City.translation_fields
    required_languages = {'default': ()}


@register(models.PlaneModel)
class PlaneModelTranslationOptions(BaseTranslationOptions):
    fields = models.PlaneModel.translation_fields
    required_languages = {'default': ()}
