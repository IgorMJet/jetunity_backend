from rest_framework import serializers

from handbooks import models
from snippets.api.serializers import fields


class CitySerializer(serializers.ModelSerializer):
    """Города"""

    class Meta:
        fields = ('id', 'title')
        model = models.City


class AirportSerializer(serializers.ModelSerializer):
    """Аэропорты"""

    city = fields.PublishedRelationField(CitySerializer, many=False)

    class Meta:
        fields = ('city', 'code', 'id', 'title')
        model = models.Airport


class PlaneModelSerializer(serializers.ModelSerializer):
    """Модели самолетов"""

    class Meta:
        fields = ('id', 'title')
        model = models.PlaneModel
