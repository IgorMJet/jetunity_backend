from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'handbooks'
    verbose_name = 'Справочники'
