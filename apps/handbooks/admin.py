from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from handbooks import models
from snippets.admin.admin import BaseDictionaryModelAdmin


@admin.register(models.Airport)
class AirportAdmin(TranslationAdmin, BaseDictionaryModelAdmin):
    """Аэропорты"""

    list_display = (
        'id', 'title', 'city', 'code', 'status', 'ordering', 'created'
    )
    list_filter = BaseDictionaryModelAdmin.list_filter + ('city',)
    search_fields = BaseDictionaryModelAdmin.search_fields + [
        'code', 'city__title'
    ]


@admin.register(models.City)
class CityAdmin(TranslationAdmin, BaseDictionaryModelAdmin):
    """Города"""
    pass


@admin.register(models.PlaneModel)
class PlaneModelAdmin(TranslationAdmin, BaseDictionaryModelAdmin):
    """Модели самолетов"""

    search_fields = BaseDictionaryModelAdmin.search_fields + ['code']
