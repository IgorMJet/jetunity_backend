# Generated by Django 3.0.11 on 2020-11-30 14:11

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vars', '0010_jetsharingbookingemailsettings_jetsharingbookingemailsettingsinfosection_useremailsettings'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('ordering', models.IntegerField(db_index=True, default=0, verbose_name='Порядок')),
                ('status', models.SmallIntegerField(choices=[(0, 'Черновик'), (1, 'Публичный'), (2, 'Скрытый')], default=1, verbose_name='Статус')),
                ('message', ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Сообщение')),
                ('message_ru', ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='Сообщение')),
                ('message_en', ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name='Сообщение')),
                ('button_caption', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кнопки')),
                ('button_caption_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кнопки')),
                ('button_caption_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст кнопки')),
                ('button_link', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кнопки')),
                ('button_link_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кнопки')),
                ('button_link_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Ссылка кнопки')),
            ],
            options={
                'verbose_name': 'Быстрое оповещение',
                'verbose_name_plural': 'Быстрые оповещения',
                'ordering': ('ordering', '-created'),
            },
        ),
    ]
