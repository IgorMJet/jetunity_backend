from django.conf import settings

from modeltranslation.decorators import register

from snippets.utils.modeltranslation import BaseTranslationOptions
from vars import models


@register(models.BookingEmailSettings)
class BookingEmailSettingsTranslationOptions(BaseTranslationOptions):
    fields = models.BookingEmailSettings.translation_fields
    required_languages = {'default': ()}


@register(models.BookingEmailSettingsInfoSection)
class BookingEmailSettingsInfoSectionTranslationOptions(BaseTranslationOptions):
    fields = models.BookingEmailSettingsInfoSection.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingBookingEmailSettings)
class JetSharingBookingEmailSettingsTranslationOptions(BaseTranslationOptions):
    fields = models.JetSharingBookingEmailSettings.translation_fields
    required_languages = {'default': ()}


@register(models.JetSharingBookingEmailSettingsInfoSection)
class JetSharingBookingEmailSettingsInfoSectionTranslationOptions(
        BaseTranslationOptions):
    fields = models.BookingEmailSettingsInfoSection.translation_fields
    required_languages = {'default': ()}


@register(models.Menu)
class MenuTranslationOptions(BaseTranslationOptions):
    fields = models.Menu.translation_fields
    required_languages = {'default': ()}


@register(models.MenuItem)
class MenuItemTranslationOptions(BaseTranslationOptions):
    fields = models.MenuItem.translation_fields
    required_languages = {settings.DEFAULT_LANGUAGE: ('url',), 'default': ()}


@register(models.PartnerEmailSettings)
class PartnerEmailSettingsTranslationOptions(BaseTranslationOptions):
    fields = models.PartnerEmailSettings.translation_fields
    required_languages = {'default': ()}


@register(models.SiteConfig)
class SiteConfigTranslationOptions(BaseTranslationOptions):
    fields = models.SiteConfig.translation_fields
    required_languages = {'default': ()}


@register(models.TopMessage)
class TopMessageTranslationOptions(BaseTranslationOptions):
    fields = models.TopMessage.translation_fields
    required_languages = {'default': ()}


@register(models.UserEmailSettings)
class UserEmailSettingsTranslationOptions(BaseTranslationOptions):
    fields = models.UserEmailSettings.translation_fields
    required_languages = {'default': ()}
