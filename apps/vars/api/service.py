from django.conf import settings


def get_default_admins():
    return ','.join([admin[1] for admin in settings.ADMINS])


def is_old_prices_on():
    from vars.site_config import site_config
    return site_config.get('is_old_price', False)
