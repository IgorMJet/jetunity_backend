from django.db.models import Prefetch

from snippets.api.views import PublicViewMixin, BaseRetrieveAPIView
from vars import models
from vars.api import serializers


class SettingsView(PublicViewMixin, BaseRetrieveAPIView):
    """Настройки"""

    serializer_class = serializers.SettingsSerializer

    def get_object(self):

        return {
            'menus': models.Menu.objects.published().prefetch_related(
                Prefetch(
                    'items',
                    queryset=models.MenuItem.objects.published().select_related('child_menu'),
                    to_attr='cached_items'
                )
            ),
            'settings': models.SiteConfig.get_solo()
        }
