from rest_framework import serializers

from pages.api.serializers import FlatPageShortSerializer
from snippets.api.serializers import fields
from snippets.api.serializers.mixins import CommonSerializer
from vars import models


class MenuItemSerializer(serializers.ModelSerializer):
    """Пункты меню"""

    child_menu = serializers.SerializerMethodField()

    class Meta:
        model = models.MenuItem
        fields = ('child_menu', 'class_name', 'id', 'title', 'url')

    @staticmethod
    def get_child_menu(obj):
        if obj.child_menu_id:
            return MenuSerializer(obj.child_menu).data


class MenuSerializer(serializers.ModelSerializer):
    """Меню"""

    items = serializers.SerializerMethodField()

    class Meta:
        model = models.Menu
        fields = ('items', 'link', 'slug', 'title')

    @staticmethod
    def get_items(obj):
        items = obj.cached_items\
            if hasattr(obj, 'cached_items') else obj.items.published()

        return MenuItemSerializer(items, many=True).data


class TopMessageSerializer(serializers.ModelSerializer):
    """Быстрые оповещения"""

    color = serializers.SerializerMethodField()

    class Meta:
        model = models.TopMessage
        fields = ('button_caption', 'button_link', 'color', 'id', 'message')

    @staticmethod
    def get_color(obj):
        return obj.color if obj.color else None


class SiteConfigSerializer(serializers.ModelSerializer):
    """Настройки сайта"""

    cookie_bar_text = fields.PretextField()
    copyrights = fields.PretextField()
    logo = fields.ImageField()
    logo_dark = fields.ImageField()
    privacy_policy = fields.PublishedRelationField(
        FlatPageShortSerializer, many=False
    )
    top_message = serializers.SerializerMethodField()

    class Meta:
        model = models.SiteConfig
        fields = (
            'agree_text', 'cookie_bar_text', 'copyrights',
            'form_socials_title', 'logo', 'logo_dark', 'phone', 'phone_2',
            'privacy_policy', 'public_email', 'share_content', 'share_title',
            'top_message'
        )

    @staticmethod
    def get_top_message(obj):
        msg = models.TopMessage.objects.published()\
            .order_by('ordering', '-created').first()
        if msg:
            return TopMessageSerializer(msg, many=False).data
        return None


class SettingsSerializer(CommonSerializer):
    """Настройки"""

    menus = MenuSerializer(many=True)
    settings = SiteConfigSerializer()
