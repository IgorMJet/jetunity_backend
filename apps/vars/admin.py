from django.contrib import admin
from django.utils.html import format_html

from modeltranslation.admin import TranslationStackedInline, TranslationAdmin
from solo.admin import SingletonModelAdmin

from snippets.admin import SuperUserDeletableAdminMixin, BaseModelAdmin
from snippets.utils.modeltranslation import get_model_translation_fields
from vars import models


class BookingEmailSettingsInfoSectionInline(TranslationStackedInline):
    """Блоки информации настройки письма после бронирования"""

    extra = 0
    fields = models.BookingEmailSettingsInfoSection().collect_fields()
    model = models.BookingEmailSettingsInfoSection
    ordering = ('ordering',)
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-info'


@admin.register(models.BookingEmailSettings)
class BookingEmailSettingsAdmin(SingletonModelAdmin, TranslationAdmin):
    """Настройки письма после бронирования"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': ('subject', 'title', 'bg', 'greeting', 'content')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-details'),
            'fields': ('show_details', 'details_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-partnership'),
            'fields': (
                'show_partnership', 'partnership_title', 'partnership_content',
                'partnership_image', 'partnership_url'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-info'),
            'fields': ('show_info', 'info_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-contacts'),
            'fields': (
                'show_contacts', 'contacts_title', 'contacts_content',
                'contacts_phone_1', 'contacts_phone_2', 'contacts_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-app'),
            'fields': (
                'show_app', 'app_title', 'app_image',
                'app_apple_appstore_link', 'app_google_play_link'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-socials'),
            'fields': ('show_socials', 'socials_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-footer'),
            'fields': (
                'show_bottom_menu', 'bottom_menu', 'copyrights',
                'unsubscribe_text'
            )
        })
    ]
    inlines = (BookingEmailSettingsInfoSectionInline,)
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('details', 'Детализация'),
        ('partnership', 'Партнерство'),
        ('info', 'Информация'),
        ('contacts', 'Контакты'),
        ('app', 'Приложение'),
        ('socials', 'Соцсети'),
        ('footer', 'Подвал')
    )


class JetSharingBookingEmailSettingsInfoSectionInline(
        TranslationStackedInline):
    """Блоки информации настройки письма после бронирования JetSharing"""

    extra = 0
    fields = models.JetSharingBookingEmailSettingsInfoSection()\
        .collect_fields()
    model = models.JetSharingBookingEmailSettingsInfoSection
    ordering = ('ordering',)
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-info'


@admin.register(models.JetSharingBookingEmailSettings)
class JetSharingBookingEmailSettingsAdmin(SingletonModelAdmin,
                                          TranslationAdmin):
    """Настройки письма после бронирования JetSharing"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': ('subject', 'title', 'bg', 'greeting', 'content')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-details'),
            'fields': ('show_details', 'details_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-partnership'),
            'fields': (
                'show_partnership', 'partnership_title', 'partnership_content',
                'partnership_image', 'partnership_url'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-info'),
            'fields': ('show_info', 'info_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-contacts'),
            'fields': (
                'show_contacts', 'contacts_title', 'contacts_content',
                'contacts_phone_1', 'contacts_phone_2', 'contacts_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-app'),
            'fields': (
                'show_app', 'app_title', 'app_image',
                'app_apple_appstore_link', 'app_google_play_link'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-socials'),
            'fields': ('show_socials', 'socials_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-footer'),
            'fields': (
                'show_bottom_menu', 'bottom_menu', 'copyrights',
                'unsubscribe_text'
            )
        })
    ]
    inlines = (JetSharingBookingEmailSettingsInfoSectionInline,)
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('details', 'Детализация'),
        ('partnership', 'Партнерство'),
        ('info', 'Информация'),
        ('contacts', 'Контакты'),
        ('app', 'Приложение'),
        ('socials', 'Соцсети'),
        ('footer', 'Подвал')
    )


class MenuItemInline(TranslationStackedInline):
    """Пункты меню"""

    extra = 0
    fields = models.MenuItem().collect_fields()
    fk_name = 'menu'
    model = models.MenuItem
    ordering = ('ordering',)
    raw_id_fields = ('child_menu',)
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-items'


@admin.register(models.Menu)
class MenuAdmin(SuperUserDeletableAdminMixin, BaseModelAdmin,
                TranslationAdmin):
    """Меню"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': models.Menu().collect_fields()
        })
    ]
    inlines = (MenuItemInline,)
    list_display = ('slug', 'work_title', 'title', 'status', 'created')
    list_editable = ('status',)
    ordering = BaseModelAdmin.ordering + ('slug',)
    search_fields = ['=id', 'link', 'slug', 'title', 'work_title'] + [
        'items__%s' % x for x in get_model_translation_fields(models.MenuItem)
    ]
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('items', 'Пункты меню')
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = tuple(self.readonly_fields)
        if request.user.is_superuser:
            return readonly_fields

        return readonly_fields + ('slug', 'work_title')


@admin.register(models.MenuItem)
class MenuItemAdmin(SuperUserDeletableAdminMixin, BaseModelAdmin,
                    TranslationAdmin):
    """Пункты меню отдельной админкой"""

    list_display = ('id', 'title', 'menu', 'status', 'ordering', 'created')
    list_display_links = ('id', 'title')
    list_filter = BaseModelAdmin.list_filter + ('menu',)
    search_fields = ['=id'] + get_model_translation_fields(models.MenuItem)


@admin.register(models.PartnerEmailSettings)
class PartnerEmailSettingsAdmin(SingletonModelAdmin, TranslationAdmin):
    """Настройки письма после отправки формы партнерства"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': ('subject', 'greeting', 'content')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': (
                'show_hero', 'hero_title', 'hero_content', 'hero_image',
                'hero_url'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-referrals'),
            'fields': (
                'show_referrals', 'referral_link_button', 'referral_title',
                'referral_content'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-contacts'),
            'fields': (
                'show_contacts', 'contacts_title', 'contacts_content',
                'contacts_phone_1', 'contacts_phone_2', 'contacts_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-app'),
            'fields': (
                'show_app', 'app_title', 'app_image',
                'app_apple_appstore_link', 'app_google_play_link'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-socials'),
            'fields': ('show_socials', 'socials_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-footer'),
            'fields': (
                'show_bottom_menu', 'bottom_menu', 'copyrights',
                'unsubscribe_text'
            )
        })
    ]
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('hero', 'Первый экран'),
        ('referrals', 'Реферральная ссылка'),
        ('contacts', 'Контакты'),
        ('app', 'Приложение'),
        ('socials', 'Соцсети'),
        ('footer', 'Подвал')
    )


@admin.register(models.SiteConfig)
class SiteConfigAdmin(SingletonModelAdmin, TranslationAdmin):
    """Настройки сайта"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': ('admins', 'logo', 'logo_dark', 'cookie_bar_text')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-contacts'),
            'fields': ('phone', 'phone_2', 'public_email')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-forms'),
            'fields': (
                'agree_text', 'privacy_policy', 'form_socials_title',
                'share_title', 'share_content'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-footer'),
            'fields': ('copyrights',)
        })
    ]
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('contacts', 'Контакты'),
        ('forms', 'Формы'),
        ('footer', 'Подвал'),
    )


@admin.register(models.TopMessage)
class TopMessageAdmin(SuperUserDeletableAdminMixin, BaseModelAdmin,
                      TranslationAdmin):
    """Быстрые оповещения"""

    fields = models.TopMessage().collect_fields()
    list_display = (
        'id', 'message_verbose', 'button_caption', 'button_link', 'status',
        'ordering', 'created'
    )
    list_display_links = ('id', 'message_verbose')
    search_fields = ['=id'] + get_model_translation_fields(models.TopMessage)

    def message_verbose(self, obj):
        if obj.message:
            return format_html(
                f'<div class="html-content">{obj.message}</div>'
            )
        return '-'

    message_verbose.short_description = 'Сообщение'
    message_verbose.admin_order_field = 'message'


@admin.register(models.UserEmailSettings)
class UserEmailSettingsAdmin(SingletonModelAdmin, TranslationAdmin):
    """Настройки письма после отправки формы регистрации клиента"""

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-basic'),
            'fields': ('subject', 'greeting', 'content')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': (
                'show_hero', 'hero_title', 'hero_content', 'hero_image',
                'hero_url'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-referrals'),
            'fields': (
                'show_referrals', 'referral_link_button', 'referral_title',
                'referral_content'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-contacts'),
            'fields': (
                'show_contacts', 'contacts_title', 'contacts_content',
                'contacts_phone_1', 'contacts_phone_2', 'contacts_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-app'),
            'fields': (
                'show_app', 'app_title', 'app_image',
                'app_apple_appstore_link', 'app_google_play_link'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-socials'),
            'fields': ('show_socials', 'socials_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-footer'),
            'fields': (
                'show_bottom_menu', 'bottom_menu', 'copyrights',
                'unsubscribe_text'
            )
        })
    ]
    suit_form_tabs = (
        ('basic', 'Основное'),
        ('hero', 'Первый экран'),
        ('referrals', 'Реферральная ссылка'),
        ('contacts', 'Контакты'),
        ('app', 'Приложение'),
        ('socials', 'Соцсети'),
        ('footer', 'Подвал')
    )
