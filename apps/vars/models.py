from colorfield.fields import ColorField
from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.html import strip_tags
from solo.models import SingletonModel

from snippets.admin import SuperUserDeletableAdminMixin
from snippets.models import BaseModel, BasicModel
from vars.api.service import get_default_admins


class BookingEmailSettings(SingletonModel, BasicModel):
    """Настройки письма после бронирования"""

    subject = models.CharField(
        'Тема письма', max_length=255, blank=True, null=True
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )
    bg = models.ImageField(
        'Фон первого экрана', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    greeting = models.CharField(
        'Приветствие', max_length=255, blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)
    show_details = models.BooleanField('Показать детализацию', default=True)
    details_title = models.CharField(
        'Детализация: заголовок', max_length=255, blank=True, null=True
    )
    show_partnership = models.BooleanField(
        'Показать блок партнерства', default=True
    )
    partnership_title = models.CharField(
        'Партнерство: заголовок', max_length=255, blank=True, null=True
    )
    partnership_content = RichTextUploadingField(
        'Партнерство: контент', blank=True, null=True
    )
    partnership_image = models.ImageField(
        'Партнерство: изображение', max_length=255, upload_to='email/images',
        blank=True,
        null=True
    )
    partnership_url = models.CharField(
        'Партнерство: ссылка', max_length=255, blank=True, null=True
    )
    show_info = models.BooleanField(
        'Показать информационный блок', default=True
    )
    info_title = models.CharField(
        'Информационный блок: заголовок', max_length=255, blank=True, null=True
    )
    show_contacts = models.BooleanField('Показать контакты', default=True)
    contacts_title = models.CharField(
        'Контакты: заголовок', max_length=255, blank=True, null=True
    )
    contacts_content = RichTextUploadingField(
        'Контакты: контент', blank=True, null=True
    )
    contacts_phone_1 = models.CharField(
        'Контакты: телефон 1', max_length=255, blank=True, null=True
    )
    contacts_phone_2 = models.CharField(
        'Контакты: телефон 2', max_length=255, blank=True, null=True
    )
    contacts_image = models.ImageField(
        'Контакты: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    show_app = models.BooleanField(
        'Показать блок мобильного приложения', default=True
    )
    app_title = models.CharField(
        'Мобильное приложение: заголовок', max_length=255, blank=True,
        null=True
    )
    app_image = models.ImageField(
        'Мобильное приложение: скриншот', max_length=255,
        upload_to='email/images', blank=True, null=True
    )
    app_apple_appstore_link = models.CharField(
        'Мобильное приложение: ссылка в Apple AppStore', max_length=255,
        blank=True, null=True
    )
    app_google_play_link = models.CharField(
        'Мобильное приложение: ссылка в Google Play', max_length=255,
        blank=True, null=True
    )
    show_socials = models.BooleanField('Показать блок соцсетей', default=True)
    socials_title = models.CharField(
        'Соцсети: заголовок', max_length=255, blank=True, null=True
    )
    show_bottom_menu = models.BooleanField(
        'Показать нижнее меню', default=True
    )
    bottom_menu = models.OneToOneField(
        'vars.Menu', verbose_name='Нижнее меню', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    copyrights = models.CharField(
        'Копирайт', max_length=255, blank=True, null=True
    )
    unsubscribe_text = RichTextUploadingField(
        'Текст отписки', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'app_image', 'app_title', 'app_apple_appstore_link',
        'app_google_play_link', 'contacts_content', 'contacts_phone_1',
        'contacts_phone_2', 'contacts_title', 'content', 'copyrights',
        'details_title', 'greeting', 'info_title', 'partnership_content',
        'partnership_title', 'partnership_url', 'socials_title', 'subject',
        'title', 'unsubscribe_text'
    )

    class Meta:
        verbose_name = 'Настройки письма после бронирования'
        verbose_name_plural = 'Настройки письма после бронирования'

    def __str__(self):
        return 'Настройки письма после бронирования'


class BookingEmailSettingsInfoSection(BaseModel):
    """Блоки информации настройки письма после бронирования"""

    settings = models.ForeignKey(
        'vars.BookingEmailSettings', verbose_name='Настройки',
        related_name='info_sections', on_delete=models.CASCADE
    )
    icon = models.FileField(
        'Иконка', max_length=255, upload_to='email/images', blank=True,
        null=True
    )
    content = models.TextField('Контент', blank=True, null=True)

    translation_fields = ('content',)

    class Meta:
        verbose_name = 'Блок информации настройки письма после бронирования'
        verbose_name_plural = 'Блоки информации настройки письма после ' \
                              'бронирования'

    def __str__(self):
        return self.content or str(self.id)


class JetSharingBookingEmailSettings(SingletonModel, BasicModel):
    """Настройки письма после бронирования JetSharing"""

    subject = models.CharField(
        'Тема письма', max_length=255, blank=True, null=True
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )
    bg = models.ImageField(
        'Фон первого экрана', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    greeting = models.CharField(
        'Приветствие', max_length=255, blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)
    show_details = models.BooleanField('Показать детализацию', default=True)
    details_title = models.CharField(
        'Детализация: заголовок', max_length=255, blank=True, null=True
    )
    show_partnership = models.BooleanField(
        'Показать блок партнерства', default=True
    )
    partnership_title = models.CharField(
        'Партнерство: заголовок', max_length=255, blank=True, null=True
    )
    partnership_content = RichTextUploadingField(
        'Партнерство: контент', blank=True, null=True
    )
    partnership_image = models.ImageField(
        'Партнерство: изображение', max_length=255, upload_to='email/images',
        blank=True,
        null=True
    )
    partnership_url = models.CharField(
        'Партнерство: ссылка', max_length=255, blank=True, null=True
    )
    show_info = models.BooleanField(
        'Показать информационный блок', default=True
    )
    info_title = models.CharField(
        'Информационный блок: заголовок', max_length=255, blank=True, null=True
    )
    show_contacts = models.BooleanField('Показать контакты', default=True)
    contacts_title = models.CharField(
        'Контакты: заголовок', max_length=255, blank=True, null=True
    )
    contacts_content = RichTextUploadingField(
        'Контакты: контент', blank=True, null=True
    )
    contacts_phone_1 = models.CharField(
        'Контакты: телефон 1', max_length=255, blank=True, null=True
    )
    contacts_phone_2 = models.CharField(
        'Контакты: телефон 2', max_length=255, blank=True, null=True
    )
    contacts_image = models.ImageField(
        'Контакты: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    show_app = models.BooleanField(
        'Показать блок мобильного приложения', default=True
    )
    app_title = models.CharField(
        'Мобильное приложение: заголовок', max_length=255, blank=True,
        null=True
    )
    app_image = models.ImageField(
        'Мобильное приложение: скриншот', max_length=255,
        upload_to='email/images', blank=True, null=True
    )
    app_apple_appstore_link = models.CharField(
        'Мобильное приложение: ссылка в Apple AppStore', max_length=255,
        blank=True, null=True
    )
    app_google_play_link = models.CharField(
        'Мобильное приложение: ссылка в Google Play', max_length=255,
        blank=True, null=True
    )
    show_socials = models.BooleanField('Показать блок соцсетей', default=True)
    socials_title = models.CharField(
        'Соцсети: заголовок', max_length=255, blank=True, null=True
    )
    show_bottom_menu = models.BooleanField(
        'Показать нижнее меню', default=True
    )
    bottom_menu = models.OneToOneField(
        'vars.Menu', verbose_name='Нижнее меню', blank=True, null=True,
        on_delete=models.SET_NULL, related_name='jetsharing_booking_settings'
    )
    copyrights = models.CharField(
        'Копирайт', max_length=255, blank=True, null=True
    )
    unsubscribe_text = RichTextUploadingField(
        'Текст отписки', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'app_image', 'app_title', 'app_apple_appstore_link',
        'app_google_play_link', 'contacts_content', 'contacts_phone_1',
        'contacts_phone_2', 'contacts_title', 'content', 'copyrights',
        'details_title', 'greeting', 'info_title', 'partnership_content',
        'partnership_title', 'partnership_url', 'socials_title', 'subject',
        'title', 'unsubscribe_text'
    )

    class Meta:
        verbose_name = 'Настройки письма после бронирования JetSharing'
        verbose_name_plural = 'Настройки письма после бронирования JetSharing'

    def __str__(self):
        return 'Настройки письма после бронирования JetSharing'


class JetSharingBookingEmailSettingsInfoSection(BaseModel):
    """Блоки информации настройки письма после бронирования JetSharing"""

    settings = models.ForeignKey(
        'vars.JetSharingBookingEmailSettings', verbose_name='Настройки',
        related_name='info_sections', on_delete=models.CASCADE
    )
    icon = models.FileField(
        'Иконка', max_length=255, upload_to='email/images', blank=True,
        null=True
    )
    content = models.TextField('Контент', blank=True, null=True)

    translation_fields = ('content',)

    class Meta:
        verbose_name = 'Блок информации настройки письма после бронирования ' \
                       'JetSharing'
        verbose_name_plural = 'Блоки информации настройки письма после ' \
                              'бронирования JetSharing'

    def __str__(self):
        return self.content or str(self.id)


class Menu(SuperUserDeletableAdminMixin, BaseModel):
    """Меню"""

    work_title = models.CharField(
        'Рабочий заголовок', max_length=255, db_index=True
    )
    title = models.CharField(
        'Заголовок', max_length=255, db_index=True, blank=True, null=True
    )
    slug = models.SlugField(
        'Алиас', db_index=True, unique=True,
        help_text='Латинские буквы и цифры, подчеркивание и дефис'
    )
    link = models.CharField('Ссылка', max_length=255, blank=True, null=True)
    translation_fields = ('link', 'title')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Меню'
        verbose_name_plural = 'Меню'

    def __str__(self):
        return self.work_title


class MenuItem(SuperUserDeletableAdminMixin, BaseModel):
    """Пункт меню"""

    menu = models.ForeignKey(
        Menu, related_name='items', verbose_name='Меню',
        on_delete=models.CASCADE
    )
    title = models.CharField('Заголовок', max_length=255)
    url = models.CharField('Ссылка', max_length=255)
    class_name = models.CharField(
        'CSS-класс для ссылки (а):', blank=True, null=True, max_length=50
    )
    child_menu = models.ForeignKey(
        'vars.Menu', verbose_name='Дочернее меню', related_name='parent_items',
        on_delete=models.SET_NULL, blank=True, null=True
    )

    translation_fields = ('title', 'url')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'

    def __str__(self):
        return f'{self.menu}: {self.title}'


class PartnerEmailSettings(SingletonModel, BasicModel):
    """Настройки письма формы партнерства"""

    subject = models.CharField(
        'Тема письма', max_length=255, blank=True, null=True
    )
    show_hero = models.BooleanField('Показывать первый экран', default=True)
    hero_title = models.CharField(
        'Первый экран: заголовок', max_length=255, blank=True, null=True
    )
    hero_content = RichTextUploadingField(
        'Первый экран: контент', blank=True, null=True
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    hero_url = models.CharField(
        'Первый экран: ссылка', max_length=255, blank=True, null=True
    )
    greeting = models.CharField(
        'Приветствие', max_length=255, blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)
    show_referrals = models.BooleanField(
        'Показать блок с реферральной ссылкой', default=True
    )
    referral_link_button = models.CharField(
        'Реферральная ссылка: текст кнопки перехода', max_length=255,
        blank=True, null=True
    )
    referral_title = models.CharField(
        'Реферральная ссылка: заголовок блока', max_length=255, blank=True,
        null=True
    )
    referral_content = RichTextUploadingField(
        'Реферральная ссылка: контент', blank=True, null=True
    )
    show_contacts = models.BooleanField('Показать контакты', default=True)
    contacts_title = models.CharField(
        'Контакты: заголовок', max_length=255, blank=True, null=True
    )
    contacts_content = RichTextUploadingField(
        'Контакты: контент', blank=True, null=True
    )
    contacts_phone_1 = models.CharField(
        'Контакты: телефон 1', max_length=255, blank=True, null=True
    )
    contacts_phone_2 = models.CharField(
        'Контакты: телефон 2', max_length=255, blank=True, null=True
    )
    contacts_image = models.ImageField(
        'Контакты: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    show_app = models.BooleanField(
        'Показать блок мобильного приложения', default=True
    )
    app_title = models.CharField(
        'Мобильное приложение: заголовок', max_length=255, blank=True,
        null=True
    )
    app_image = models.ImageField(
        'Мобильное приложение: скриншот', max_length=255,
        upload_to='email/images', blank=True, null=True
    )
    app_apple_appstore_link = models.CharField(
        'Мобильное приложение: ссылка в Apple AppStore', max_length=255,
        blank=True, null=True
    )
    app_google_play_link = models.CharField(
        'Мобильное приложение: ссылка в Google Play', max_length=255,
        blank=True, null=True
    )
    show_socials = models.BooleanField('Показать блок соцсетей', default=True)
    socials_title = models.CharField(
        'Соцсети: заголовок', max_length=255, blank=True, null=True
    )
    show_bottom_menu = models.BooleanField(
        'Показать нижнее меню', default=True
    )
    bottom_menu = models.OneToOneField(
        'vars.Menu', verbose_name='Нижнее меню', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    copyrights = models.CharField(
        'Копирайт', max_length=255, blank=True, null=True
    )
    unsubscribe_text = RichTextUploadingField(
        'Текст отписки', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'app_apple_appstore_link', 'app_google_play_link', 'app_image',
        'app_title', 'contacts_content', 'contacts_image', 'contacts_phone_1',
        'contacts_phone_2', 'contacts_title', 'content', 'copyrights',
        'greeting', 'hero_content', 'hero_title', 'hero_url',
        'referral_content', 'referral_link_button', 'referral_title',
        'socials_title', 'subject', 'unsubscribe_text'
    )

    class Meta:
        verbose_name = 'Настройки письма формы партнерства'
        verbose_name_plural = 'Настройки письма формы партнерства'

    def __str__(self):
        return 'Настройки письма формы партнерства'


class SiteConfig(SingletonModel, BasicModel):
    """Настройки сайта"""

    admins = models.TextField(
        'Администраторы сайта', default=get_default_admins,
        help_text='через запятую, без пробелов'
    )
    logo = models.FileField(
        'Логотип', upload_to='logos', max_length=255, blank=True, null=True
    )
    logo_dark = models.FileField(
        'Логотип темный', upload_to='logos', max_length=255, blank=True,
        null=True
    )
    phone = models.CharField('Телефон', max_length=100, blank=True, null=True)
    phone_2 = models.CharField(
        'Телефон 2', max_length=100, blank=True, null=True
    )
    public_email = models.EmailField(
        'Публичный email', max_length=254, blank=True, null=True
    )
    copyrights = models.TextField('Копирайт', blank=True, null=True)
    agree_text = models.TextField(
        'Текст соглашения о конфиденциальности под формой', max_length=255,
        blank=True, null=True
    )
    privacy_policy = models.OneToOneField(
        'pages.FlatPage', verbose_name='Политика конфиденциальности',
        related_name='site_config', on_delete=models.SET_NULL, blank=True,
        null=True
    )
    form_socials_title = models.CharField(
        'Форма: заголовок блока соцсетей', max_length=255, blank=True,
        null=True
    )
    share_title = models.CharField(
        'Заголовок реферрального блока при публикации', max_length=255,
        blank=True, null=True
    )
    share_content = models.TextField(
        'Текст реферрального блока при публикации', blank=True, null=True
    )
    cookie_bar_text = models.TextField(
        'Текст о политике конфиденциальности для cookie-bar', max_length=255,
        null=True
    )

    translation_fields = (
        'agree_text', 'cookie_bar_text', 'copyrights', 'form_socials_title',
        'phone', 'phone_2', 'public_email', 'share_content', 'share_title'
    )

    class Meta:
        verbose_name = 'Настройки сайта'
        verbose_name_plural = 'Настройки сайта'

    def __str__(self):
        return 'Настройки сайта'


class TopMessage(BaseModel):
    """Быстрые оповещения"""

    message = RichTextUploadingField('Сообщение')
    button_caption = models.CharField(
        'Текст кнопки', max_length=255, blank=True, null=True
    )
    button_link = models.CharField(
        'Ссылка кнопки', max_length=255, blank=True, null=True
    )
    color = ColorField('Цвет фона и текста кнопки', blank=True, null=True)

    translation_fields = ('button_caption', 'button_link', 'message')

    class Meta:
        ordering = ('ordering', '-created')
        verbose_name = 'Быстрое оповещение'
        verbose_name_plural = 'Быстрые оповещения'

    def __str__(self):
        return strip_tags(self.message)[:50]


class UserEmailSettings(SingletonModel, BasicModel):
    """Настройки письма формы регистрации пользователя"""

    subject = models.CharField(
        'Тема письма', max_length=255, blank=True, null=True
    )
    show_hero = models.BooleanField('Показывать первый экран', default=True)
    hero_title = models.CharField(
        'Первый экран: заголовок', max_length=255, blank=True, null=True
    )
    hero_content = RichTextUploadingField(
        'Первый экран: контент', blank=True, null=True
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    hero_url = models.CharField(
        'Первый экран: ссылка', max_length=255, blank=True, null=True
    )
    greeting = models.CharField(
        'Приветствие', max_length=255, blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)
    show_referrals = models.BooleanField(
        'Показать блок с реферральной ссылкой', default=True
    )
    referral_link_button = models.CharField(
        'Реферральная ссылка: текст кнопки перехода', max_length=255,
        blank=True, null=True
    )
    referral_title = models.CharField(
        'Реферральная ссылка: заголовок блока', max_length=255, blank=True,
        null=True
    )
    referral_content = RichTextUploadingField(
        'Реферральная ссылка: контент', blank=True, null=True
    )
    show_contacts = models.BooleanField('Показать контакты', default=True)
    contacts_title = models.CharField(
        'Контакты: заголовок', max_length=255, blank=True, null=True
    )
    contacts_content = RichTextUploadingField(
        'Контакты: контент', blank=True, null=True
    )
    contacts_phone_1 = models.CharField(
        'Контакты: телефон 1', max_length=255, blank=True, null=True
    )
    contacts_phone_2 = models.CharField(
        'Контакты: телефон 2', max_length=255, blank=True, null=True
    )
    contacts_image = models.ImageField(
        'Контакты: изображение', max_length=255, upload_to='email/images',
        blank=True, null=True
    )
    show_app = models.BooleanField(
        'Показать блок мобильного приложения', default=True
    )
    app_title = models.CharField(
        'Мобильное приложение: заголовок', max_length=255, blank=True,
        null=True
    )
    app_image = models.ImageField(
        'Мобильное приложение: скриншот', max_length=255,
        upload_to='email/images', blank=True, null=True
    )
    app_apple_appstore_link = models.CharField(
        'Мобильное приложение: ссылка в Apple AppStore', max_length=255,
        blank=True, null=True
    )
    app_google_play_link = models.CharField(
        'Мобильное приложение: ссылка в Google Play', max_length=255,
        blank=True, null=True
    )
    show_socials = models.BooleanField('Показать блок соцсетей', default=True)
    socials_title = models.CharField(
        'Соцсети: заголовок', max_length=255, blank=True, null=True
    )
    show_bottom_menu = models.BooleanField(
        'Показать нижнее меню', default=True
    )
    bottom_menu = models.OneToOneField(
        'vars.Menu', verbose_name='Нижнее меню', blank=True, null=True,
        on_delete=models.SET_NULL
    )
    copyrights = models.CharField(
        'Копирайт', max_length=255, blank=True, null=True
    )
    unsubscribe_text = RichTextUploadingField(
        'Текст отписки', max_length=255, blank=True, null=True
    )

    translation_fields = (
        'app_apple_appstore_link', 'app_google_play_link', 'app_image',
        'app_title', 'contacts_content', 'contacts_image', 'contacts_phone_1',
        'contacts_phone_2', 'contacts_title', 'content', 'copyrights',
        'greeting', 'hero_content', 'hero_title', 'hero_url',
        'referral_content', 'referral_link_button', 'referral_title',
        'socials_title', 'subject', 'unsubscribe_text'
    )

    class Meta:
        verbose_name = 'Настройки письма формы регистрации клиента'
        verbose_name_plural = 'Настройки письма формы регистрации клиента'

    def __str__(self):
        return 'Настройки письма формы партнерства'
