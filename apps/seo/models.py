from django.db import models

from seo.choices import RedirectCodeChoices
from snippets.models import BaseModel


class Redirect(BaseModel):
    """Редиректы"""
    from_url = models.CharField('С ссылки', max_length=255, unique=True)
    to_url = models.CharField('На ссылку', max_length=255)
    status_code = models.PositiveSmallIntegerField(
        'Статус', choices=RedirectCodeChoices.choices, default=RedirectCodeChoices.C301
    )

    class Meta:
        verbose_name = 'Редирект'
        verbose_name_plural = 'Редиректы'
