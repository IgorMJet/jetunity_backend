from rest_framework import serializers

from seo import models


class RedirectSerializer(serializers.ModelSerializer):
    """Редиректы"""

    class Meta:
        model = models.Redirect
        fields = ('from_url', 'id', 'status_code', 'to_url')
