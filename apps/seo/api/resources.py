from django.http import Http404, JsonResponse
from django.views import View

from seo import models
from seo.api import serializers
from seo.cache import redirects_cache
from snippets.api.views import PublicViewMixin, BaseListAPIView


class RedirectsListView(PublicViewMixin, BaseListAPIView):
    """Редиректы"""
    queryset = models.Redirect.objects.published()
    serializer_class = serializers.RedirectSerializer


class HasRedirectView(View):
    """Проверка редиректа"""
    @staticmethod
    def get(request, **kwargs):
        url = request.GET.get('url')

        if not url:
            raise Http404

        res = redirects_cache.get(url)
        if not res:
            raise Http404

        return JsonResponse({'to_url': res[0], 'status': res[1]})
