from django.urls import path

from seo.api import resources


app_name = 'seo'

urlpatterns = (
    path(
        'redirects/',
        resources.RedirectsListView.as_view(),
        name='redirects'
    ),
    path(
        'has-redirect/',
        resources.HasRedirectView.as_view(),
        name='has_redirect'
    )
)
