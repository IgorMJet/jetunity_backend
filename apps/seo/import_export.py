from import_export import resources

from seo import models


class RedirectResource(resources.ModelResource):
    class Meta:
        fields = ('id', 'from_url', 'to_url', 'status_code', 'created')
        export_order = fields[:]
        model = models.Redirect
        skip_unchanged = True
