from django.contrib import admin

from import_export.admin import ExportMixin
from import_export.formats import base_formats

from seo import models, import_export
from snippets.admin import BaseModelAdmin


@admin.register(models.Redirect)
class RedirectAdmin(ExportMixin, BaseModelAdmin):
    """Редиректы"""
    formats = [
        base_formats.CSV, base_formats.XLS, base_formats.HTML, base_formats.ODS, base_formats.TSV
    ]
    list_display = ('from_url', 'to_url', 'status_code', 'status', 'created')
    list_display_links = ('from_url', 'to_url')
    list_editable = ('status',)
    list_filter = ('status', 'status_code')
    list_select_related = True
    resource_class = import_export.RedirectResource
    search_fields = ('from_url', 'to_url')
