from modeltranslation.decorators import register

from pages import models
from snippets.utils.modeltranslation import BaseTranslationOptions


@register(models.Branch)
class BranchTranslationOptions(BaseTranslationOptions):
    fields = models.Branch.translation_fields
    required_languages = {'default': ()}


@register(models.ContactsPage)
class ContactsPageTranslationOptions(BaseTranslationOptions):
    fields = models.ContactsPage.translation_fields
    required_languages = {'default': ()}


@register(models.FlatPage)
class FlatPageTranslationOptions(BaseTranslationOptions):
    fields = models.FlatPage.translation_fields
    required_languages = {'default': ()}


@register(models.FlatPageSection)
class FlatPageSectionTranslationOptions(BaseTranslationOptions):
    fields = models.FlatPageSection.translation_fields
    required_languages = {'default': ()}


@register(models.HomePage)
class HomePageTranslationOptions(BaseTranslationOptions):
    fields = models.HomePage.translation_fields
    required_languages = {'default': ()}


@register(models.HomePageFeature)
class HomePageFeatureTranslationOptions(BaseTranslationOptions):
    fields = models.HomePageFeature.translation_fields
    required_languages = {'default': ()}


@register(models.HomePageServiceSection)
class HomePageServiceSectionTranslationOptions(BaseTranslationOptions):
    fields = models.HomePageServiceSection.translation_fields
    required_languages = {'default': ()}


@register(models.HomePageSlide)
class HomePageSlideTranslationOptions(BaseTranslationOptions):
    fields = models.HomePageSlide.translation_fields
    required_languages = {'default': ()}
