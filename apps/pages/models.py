from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField

from core.models import ReferralCodeFormPageMixin
from snippets.models import BaseModel
from snippets.models.pages import BasePage
from snippets.models.seo import SEOModelMixin


class Branch(BaseModel):
    """Филиалы"""

    page = models.ForeignKey(
        'pages.ContactsPage', verbose_name='Страница', related_name='branches',
        on_delete=models.CASCADE
    )
    title = models.CharField('Название', max_length=255, blank=True, null=True)
    phone = models.CharField('Телефон', max_length=255, blank=True, null=True)
    email = models.EmailField('Email', max_length=254, blank=True, null=True)
    city = models.CharField('Город', max_length=255, blank=True, null=True)
    address = models.TextField('Адрес', max_length=255, blank=True, null=True)
    map_lat = models.FloatField('Карта: широта', blank=True, null=True)
    map_lon = models.FloatField('Карта: долгота', blank=True, null=True)

    translation_fields = ('address', 'city', 'email', 'phone', 'title')

    class Meta:
        verbose_name = 'Страница контактов'
        verbose_name_plural = 'Страница контактов'

    def __str__(self):
        return 'Страница контактов'


class ContactsPage(BasePage):
    """Страница контактов"""

    hero_title = models.CharField(
        'Первый экран: заголовок', max_length=255, blank=True, null=True
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='contacts',
        blank=True, null=True
    )
    slogan = models.TextField('Слоган', blank=True, null=True)
    show_socials = models.BooleanField('Показывать соцсети', default=True)

    translation_fields = BasePage.translation_fields + ('hero_title', 'slogan')

    class Meta:
        verbose_name = 'Страница контактов'
        verbose_name_plural = 'Страница контактов'

    def __str__(self):
        return 'Страница контактов'


class FlatPage(SEOModelMixin, BaseModel):
    """Простая страница"""

    title = models.CharField(
        'Заголовок (левая часть)', max_length=255, blank=True, null=True
    )
    title_right = models.CharField(
        'Заголовок (правая часть)', max_length=255, blank=True, null=True
    )
    slug = models.SlugField(
        'Алиас', max_length=150, db_index=True, unique=True,
        help_text='Латинские буквы и цифры, подчеркивание и дефис'
    )
    hero_image = models.ImageField(
        'Первый экран: изображение', max_length=255, upload_to='pages',
        blank=True, null=True
    )
    content = RichTextUploadingField('Контент', blank=True, null=True)

    translation_fields = SEOModelMixin.translation_fields + (
        'content', 'hero_image', 'title', 'title_right'
    )

    class Meta:
        ordering = ('ordering', 'title')
        verbose_name = 'Простая страница'
        verbose_name_plural = 'Простые страницы'

    def __str__(self):
        return f'{self.title or ""} {self.title_right or ""}'

    def get_absolute_url(self):
        return f'/pages/{self.slug}'


class FlatPageSection(BaseModel):
    """Секции простой страницы"""

    page = models.ForeignKey(
        'pages.FlatPage', verbose_name='Страница', on_delete=models.CASCADE,
        related_name='sections'
    )
    title = models.TextField('Заголовок', max_length=255, blank=True, null=True)
    content = RichTextUploadingField('Контент: колонка 1', blank=True, null=True)

    translation_fields = ('content', 'title')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Секция простой страницы'
        verbose_name_plural = 'Секции простой страницы'


class HomePage(ReferralCodeFormPageMixin, BasePage):
    """Главная страница"""

    hero_title = models.CharField(
        'Первый экран: заголовок', max_length=255, blank=True, null=True
    )
    show_hero_socials = models.BooleanField('Показывать соцсети', default=True)
    hero_bg = models.ImageField(
        'Первый экран: фон', max_length=255, upload_to='home/images',
        blank=True, null=True
    )
    show_form = models.BooleanField('Показывать блок формы', default=True)
    form_subtitle = models.CharField(
        'Форма: надпись над формой', max_length=255, blank=True, null=True
    )
    show_about = models.BooleanField('Показывать блок о сервисе', default=True)
    about_title = models.CharField(
        'О сервисе: заголовок', max_length=255, blank=True, null=True
    )
    about_indicator = models.CharField(
        'О сервисе: показатель', max_length=255, blank=True, null=True
    )
    about_content = RichTextUploadingField(
        'О сервисе: контент', blank=True, null=True
    )
    about_image = models.ImageField(
        'О сервисе: изображение', max_length=255, upload_to='home/images',
        blank=True, null=True
    )
    show_services = models.BooleanField('Показывать услуги', default=True)
    services_title = models.CharField(
        'Услуги: заголовок', max_length=255, blank=True, null=True
    )
    services_image_1 = models.ImageField(
        'Услуги: изображение 1', max_length=255, upload_to='home/images',
        blank=True, null=True
    )
    services_image_2 = models.ImageField(
        'Услуги: изображение 2', max_length=255, upload_to='home/images',
        blank=True, null=True
    )
    services_image_3 = models.ImageField(
        'Услуги: изображение 3', max_length=255, upload_to='home/images',
        blank=True, null=True
    )
    services_description_title = models.CharField(
        'Услуги: заголовок блока под изображениями', max_length=255,
        blank=True, null=True
    )
    services_description_title_link = models.CharField(
        'Услуги: ссылка заголовка блока под изображениями', max_length=255,
        blank=True, null=True
    )
    services_description_content = RichTextUploadingField(
        'Услуги: контент блока под изображениями', blank=True, null=True
    )
    show_service_sections = models.BooleanField(
        'Показывать секции услуг', default=True
    )
    show_features = models.BooleanField('Показывать возможности', default=True)
    features_title = models.CharField(
        'Возможности: заголовок', max_length=255, blank=True, null=True
    )
    show_slides = models.BooleanField('Показывать слайды', default=True)
    slides_title = models.CharField(
        'Слайды: заголовок', max_length=255, blank=True, null=True
    )
    show_mobile_app = models.BooleanField(
        'Показывать блок о мобильном приложении', default=True
    )
    mobile_app_title = models.CharField(
        'Мобильное приложение: заголовок', max_length=255, blank=True,
        null=True
    )
    mobile_app_content = RichTextUploadingField(
        'Мобильное приложение: контент', blank=True, null=True
    )
    mobile_app_apple_appstore_link = models.URLField(
        'Мобильное приложение: ссылка в Apple Appstore', max_length=255,
        blank=True, null=True
    )
    mobile_app_google_play_link = models.URLField(
        'Мобильное приложение: ссылка в Google Play', max_length=255,
        blank=True, null=True
    )
    mobile_app_screenshots_image = models.ImageField(
        'Мобильное приложение: изображение скриншотов', max_length=255,
        upload_to='home/images',
        blank=True, null=True
    )
    mobile_app_bg = models.ImageField(
        'Мобильное приложение: фон', max_length=255, upload_to='home/images',
        blank=True, null=True
    )

    translation_fields = BasePage.translation_fields + (
        'about_content', 'about_indicator', 'about_title', 'features_title',
        'form_copy_button', 'form_sent_to_email', 'form_share_button',
        'form_subtitle', 'form_success_message_content',
        'form_success_message_title', 'hero_title',
        'mobile_app_apple_appstore_link', 'mobile_app_content',
        'mobile_app_google_play_link', 'mobile_app_screenshots_image',
        'mobile_app_title', 'services_description_content',
        'services_description_title', 'services_description_title_link',
        'services_title', 'slides_title'
    )

    class Meta:
        verbose_name = 'Главная страница'
        verbose_name_plural = 'Главная страница'

    def __str__(self):
        return 'Главная страница'

    @staticmethod
    def get_absolute_url():
        return '/'


class HomePageFeature(BaseModel):
    """Возможности на главной"""

    page = models.ForeignKey(
        'pages.HomePage', verbose_name='Страница', related_name='features',
        on_delete=models.CASCADE
    )
    icon = models.FileField(
        'Иконка', max_length=255, upload_to='home/features', blank=True,
        null=True
    )
    description = RichTextUploadingField('Описание', blank=True, null=True)

    translation_fields = ('description',)

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Возможность на главной'
        verbose_name_plural = 'Возможности на главной'

    def __str__(self):
        return str(self.pk)


class HomePageServiceSection(BaseModel):
    """Секции услуг на главной"""

    page = models.ForeignKey(
        'pages.HomePage', verbose_name='Страница',
        related_name='service_sections', on_delete=models.CASCADE
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )
    link = models.CharField('Ссылка', max_length=255, blank=True, null=True)
    content = RichTextUploadingField('Контент', blank=True, null=True)
    image = models.ImageField(
        'Изображение', max_length=255, upload_to='home/services', blank=True,
        null=True
    )

    translation_fields = ('content', 'link', 'title')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Секция услуги на главной'
        verbose_name_plural = 'Секции услуг на главной'

    def __str__(self):
        return self.title or str(self.id)


class HomePageSlide(BaseModel):
    """Слайды на главной"""

    page = models.ForeignKey(
        'pages.HomePage', verbose_name='Страница', related_name='slides',
        on_delete=models.CASCADE
    )
    title = models.CharField(
        'Заголовок', max_length=255, blank=True, null=True
    )
    link = models.CharField('Ссылка', max_length=255, blank=True, null=True)
    content = RichTextUploadingField('Контент', blank=True, null=True)
    image = models.ImageField(
        'Изображение', max_length=255, upload_to='home/slides', blank=True,
        null=True
    )

    translation_fields = ('content', 'link', 'title')

    class Meta:
        ordering = ('ordering',)
        verbose_name = 'Слайд на главной'
        verbose_name_plural = 'Слайды на главной'

    def __str__(self):
        return self.title or str(self.id)
