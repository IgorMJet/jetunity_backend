from pages import models
from pages.api import serializers
from snippets.api.views import PublicViewMixin, PageRetrieveAPIView, BaseRetrieveAPIView, \
    BaseListAPIView


class ContactsPageView(PublicViewMixin, PageRetrieveAPIView):
    """Страница контактов"""

    queryset = models.ContactsPage.objects.all()
    serializer_class = serializers.ContactsPageSerializer


class FlatPageView(PublicViewMixin, BaseRetrieveAPIView):
    """Простая страница"""

    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
    queryset = models.FlatPage.objects.published()
    serializer_class = serializers.FlatPageSerializer


class FlatPagesListView(PublicViewMixin, BaseListAPIView):
    """Список простых страниц"""

    queryset = models.FlatPage.objects.published()
    serializer_class = serializers.FlatPageShortSerializer


class HomePageView(PublicViewMixin, PageRetrieveAPIView):
    """Главная страница"""

    queryset = models.HomePage.objects.all()
    serializer_class = serializers.HomePageSerializer
