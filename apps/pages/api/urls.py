from django.urls import path

from pages.api import resources


app_name = 'pages'

urlpatterns = (
    path(
        'contacts/',
        resources.ContactsPageView.as_view(),
        name='contacts_page'
    ),
    path(
        'flatpages/',
        resources.FlatPagesListView.as_view(),
        name='flatpages'
    ),
    path(
        'flatpages/<slug:slug>/',
        resources.FlatPageView.as_view(),
        name='flatpage'
    ),
    path(
        'homepage/',
        resources.HomePageView.as_view(),
        name='home'
    )
)
