from rest_framework import serializers

from pages import models
from snippets.api.serializers import fields
from snippets.api.serializers.mixins import SEOSerializerMixin


class BranchSerializer(serializers.ModelSerializer):
    """Филиалы"""

    address = fields.PretextField()

    class Meta:
        fields = (
            'address', 'city', 'email', 'id', 'map_lat', 'map_lon', 'phone',
            'title'
        )
        model = models.Branch


class ContactsPageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Страница контактов"""

    branches = fields.PublishedRelationField(BranchSerializer, many=True)
    hero_image = fields.ImageField()
    meta = serializers.SerializerMethodField()
    slogan = fields.PretextField()

    class Meta:
        model = models.ContactsPage
        fields = (
            'branches', 'hero_image', 'hero_title', 'meta', 'show_socials',
            'slogan', 'title'
        )


class FlatPageSectionSerializer(serializers.ModelSerializer):
    """Секции простых страниц"""

    title = fields.PretextField()

    class Meta:
        fields = ('content', 'id', 'title')
        model = models.FlatPageSection


class FlatPageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Простые страницы"""

    hero_image = fields.ImageField()
    meta = serializers.SerializerMethodField()
    sections = fields.PublishedRelationField(FlatPageSectionSerializer, many=True)

    class Meta:
        model = models.FlatPage
        fields = (
            'content', 'hero_image', 'id', 'meta', 'sections', 'slug', 'title', 'title_right'
        )


class FlatPageListSerializer(serializers.ModelSerializer):
    """Простые страницы списком"""

    class Meta:
        model = models.FlatPage
        fields = ('id', 'slug', 'title')


class FlatPageShortSerializer(serializers.ModelSerializer):
    """Простые страницы коротко"""

    class Meta:
        model = models.FlatPage
        fields = ('id', 'slug')


class HomePageFeatureSerializer(serializers.ModelSerializer):
    """Возможности на главной"""

    icon = fields.ImageField()

    class Meta:
        fields = ('description', 'icon', 'id')
        model = models.HomePageFeature


class HomePageServiceSectionSerializer(serializers.ModelSerializer):
    """Секции услуг на главной"""

    image = fields.ImageField()

    class Meta:
        fields = ('content', 'id', 'image', 'link', 'title')
        model = models.HomePageServiceSection


class HomePageSlideSerializer(serializers.ModelSerializer):
    """Слайды на главной"""

    image = fields.ImageField()

    class Meta:
        fields = ('content', 'id', 'image', 'link', 'title')
        model = models.HomePageSlide


class HomePageSerializer(SEOSerializerMixin, serializers.ModelSerializer):
    """Главная страница"""

    about_image = fields.ImageField()
    features = fields.PublishedRelationField(HomePageFeatureSerializer, many=True)
    form_sent_to_email = fields.PretextField()
    hero_bg = fields.ImageField()
    services_image_1 = fields.ImageField()
    services_image_2 = fields.ImageField()
    services_image_3 = fields.ImageField()
    meta = serializers.SerializerMethodField()
    mobile_app_screenshots_image = fields.ImageField()
    mobile_app_bg = fields.ImageField()
    service_sections = fields.PublishedRelationField(HomePageServiceSectionSerializer, many=True)
    slides = fields.PublishedRelationField(HomePageSlideSerializer, many=True)

    class Meta:
        model = models.HomePage
        fields = (
            'about_content', 'about_image', 'about_indicator', 'about_title', 'features',
            'features_title', 'form_copy_button', 'form_sent_to_email', 'form_share_button',
            'form_subtitle', 'form_success_message_content', 'form_success_message_title',
            'hero_bg', 'hero_title', 'meta', 'mobile_app_apple_appstore_link', 'mobile_app_bg',
            'mobile_app_content', 'mobile_app_google_play_link', 'mobile_app_screenshots_image',
            'mobile_app_title', 'service_sections', 'services_description_content',
            'services_description_title', 'services_description_title_link', 'services_image_1',
            'services_image_2', 'services_image_3', 'services_title', 'show_about',
            'show_features', 'show_form', 'show_hero_socials', 'show_mobile_app',
            'show_service_sections', 'show_services', 'show_slides', 'slides', 'slides_title',
            'title'
        )
