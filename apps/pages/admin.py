from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from solo.admin import SingletonModelAdmin

from pages import models
from snippets.admin import BaseModelAdmin
from snippets.utils.modeltranslation import get_model_translation_fields


class BranchInline(TranslationStackedInline):
    """Филиалы"""

    extra = 0
    fields = models.Branch().collect_fields()
    model = models.Branch
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-branches'


@admin.register(models.ContactsPage)
class ContactsPageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Страница контактов"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': ('hero_title', 'hero_image')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-branches'),
            'fields': ('slogan', 'show_socials')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (BranchInline,)
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('branches', 'Филиалы'),
        ('seo', 'SEO')
    )


class FlatPageSectionInline(TranslationStackedInline):
    """Секции простой страницы"""

    extra = 0
    fields = models.FlatPageSection().collect_fields()
    model = models.FlatPageSection
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-sections'


@admin.register(models.FlatPage)
class FlatPageAdmin(BaseModelAdmin, TranslationAdmin):
    """Простые страницы"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'title_right', 'slug', 'hero_image', 'status', 'created', 'updated'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-content'),
            'fields': ('content',)
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (FlatPageSectionInline,)
    list_display = ('title', 'title_right', 'slug', 'status', 'created')
    list_editable = ('status',)
    search_fields = ['=id', 'slug'] + get_model_translation_fields(models.FlatPage)
    suit_form_tabs = (
        ('general', 'Основное'),
        ('content', 'Контент'),
        ('sections', 'Секции'),
        ('seo', 'SEO')
    )

    class Media:
        js = ('admin/js/translit.js',)


class HomePageFeatureInline(TranslationStackedInline):
    """Возможности на главной странице"""

    extra = 0
    fields = models.HomePageFeature().collect_fields()
    model = models.HomePageFeature
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-features'


class HomePageServiceSectionInline(TranslationStackedInline):
    """Секции услуг на главной странице"""

    extra = 0
    fields = models.HomePageServiceSection().collect_fields()
    model = models.HomePageServiceSection
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-service_sections'


class HomePageSlideInline(TranslationStackedInline):
    """Слайды на главной странице"""

    extra = 0
    fields = models.HomePageSlide().collect_fields()
    model = models.HomePageSlide
    readonly_fields = ('created', 'updated')
    suit_classes = 'suit-tab suit-tab-slides'


@admin.register(models.HomePage)
class HomePageAdmin(SingletonModelAdmin, TranslationAdmin):
    """Главная страница"""

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('title', 'created', 'updated')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-hero'),
            'fields': ('hero_title', 'show_hero_socials', 'hero_bg')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-form'),
            'fields': (
                'show_form', 'form_subtitle', 'form_success_message_title',
                'form_success_message_content', 'form_share_button',
                'form_copy_button', 'form_sent_to_email'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-about'),
            'fields': (
                'show_about', 'about_title', 'about_indicator',
                'about_content', 'about_image'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-services'),
            'fields': (
                'show_services', 'services_title', 'services_image_1',
                'services_image_2', 'services_image_3',
                'services_description_title',
                'services_description_title_link',
                'services_description_content'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-service_sections'),
            'fields': ('show_service_sections',)
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-features'),
            'fields': ('show_features', 'features_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-slides'),
            'fields': ('show_slides', 'slides_title')
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-mobile_app'),
            'fields': (
                'show_mobile_app', 'mobile_app_title', 'mobile_app_content',
                'mobile_app_apple_appstore_link', 'mobile_app_google_play_link',
                'mobile_app_screenshots_image', 'mobile_app_bg'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo'),
            'fields': ('seo_title', 'seo_description')
        })
    )
    inlines = (
        HomePageFeatureInline, HomePageServiceSectionInline,
        HomePageSlideInline
    )
    readonly_fields = ('created', 'updated')
    suit_form_tabs = (
        ('general', 'Основное'),
        ('hero', 'Первый экран'),
        ('form', 'Форма'),
        ('about', 'О сервисе'),
        ('services', 'Услуги'),
        ('service_sections', 'Секции услуг'),
        ('features', 'Возможности'),
        ('slides', 'Слайды'),
        ('mobile_app', 'Мобильное приложение'),
        ('seo', 'SEO')
    )
