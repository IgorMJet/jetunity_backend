# Generated by Django 3.0.7 on 2020-07-10 16:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20200710_1905'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactsPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('seo_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='META заголовок (title)')),
                ('seo_description', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('seo_description_ru', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('seo_description_en', models.TextField(blank=True, max_length=1024, null=True, verbose_name='META описание (description)')),
                ('title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('hero_title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Первый экран: заголовок')),
                ('hero_image', models.ImageField(blank=True, max_length=255, null=True, upload_to='contacts', verbose_name='Первый экран: изображение')),
                ('slogan', models.TextField(blank=True, null=True, verbose_name='Слоган')),
                ('slogan_ru', models.TextField(blank=True, null=True, verbose_name='Слоган')),
                ('slogan_en', models.TextField(blank=True, null=True, verbose_name='Слоган')),
                ('show_socials', models.BooleanField(default=True, verbose_name='Показывать соцсети')),
            ],
            options={
                'verbose_name': 'Страница контактов',
                'verbose_name_plural': 'Страница контактов',
            },
        ),
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('ordering', models.IntegerField(db_index=True, default=0, verbose_name='Порядок')),
                ('status', models.SmallIntegerField(choices=[(0, 'Черновик'), (1, 'Публичный'), (2, 'Скрытый')], default=1, verbose_name='Статус')),
                ('title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Название')),
                ('title_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Название')),
                ('title_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Название')),
                ('phone', models.CharField(blank=True, max_length=255, null=True, verbose_name='Телефон')),
                ('phone_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Телефон')),
                ('phone_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Телефон')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email')),
                ('email_ru', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email')),
                ('email_en', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email')),
                ('city', models.CharField(blank=True, max_length=255, null=True, verbose_name='Город')),
                ('city_ru', models.CharField(blank=True, max_length=255, null=True, verbose_name='Город')),
                ('city_en', models.CharField(blank=True, max_length=255, null=True, verbose_name='Город')),
                ('address', models.TextField(blank=True, max_length=255, null=True, verbose_name='Адрес')),
                ('address_ru', models.TextField(blank=True, max_length=255, null=True, verbose_name='Адрес')),
                ('address_en', models.TextField(blank=True, max_length=255, null=True, verbose_name='Адрес')),
                ('map_lat', models.FloatField(blank=True, null=True, verbose_name='Карта: широта')),
                ('map_lon', models.FloatField(blank=True, null=True, verbose_name='Карта: долгота')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='branches', to='pages.ContactsPage', verbose_name='Страница')),
            ],
            options={
                'verbose_name': 'Страница контактов',
                'verbose_name_plural': 'Страница контактов',
            },
        ),
    ]
